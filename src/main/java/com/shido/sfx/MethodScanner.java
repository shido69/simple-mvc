/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.exceptions.InvalidControllerMethodCallException;
import com.shido.sfx.util.CommonUtils;
import java.lang.reflect.Method;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class MethodScanner {
    private static Logger logger= Logger.getLogger(MethodScanner.class);
    
    public static Method findCompatibleMethod(Class controller, Object... signals) throws InvalidControllerMethodCallException {
        Method result = null;
        Method method = findMethod(controller, signals);
        if(method==null){
            throw new InvalidControllerMethodCallException();
        }
        Class types[] = method.getParameterTypes();
        if (MethodScanner.isParamCompatible(types, signals)) {
            result = method;
        }
        return result;
    }

    public static boolean isParamCompatible(Class types[], Object[] values) {
        boolean result;
        Object[] params = getCompatibleParam(types, values);
        result = params != null;
        return result;
    }

    public static Object[] getCompatibleParam(Class types[], Object[] values) {
        logger.debug("---checking compatibility---");
        boolean isCompatible = false;
        Object params[] = new Object[values.length - 1];
        if (types.length == params.length) {
            isCompatible = true;
            for (int i = 0; i < types.length; i++) {
                Class type = types[i];
                Class valueType = isParamCompatible(type, values[i + 1]);
                boolean assignable = type.isAssignableFrom(valueType);
                isCompatible &= assignable;
                System.out.print(type.getName() + " <-> " + valueType.getName() + " is" + (assignable ? "" : " not") + " assignable ");
                if (values[i + 1] == null) {
                    params[i] = null;
                } else {
                    params[i] = CommonUtils.parseParameterValue(type, values[i + 1].toString());
                }
                logger.debug("with value " + params[i]);
            }
        }
        return isCompatible ? params : null;
    }

    

    public static Class isParamCompatible(Class type, Object value) {
        Class result = null;
        if (type != null) {
            if (type.isInstance(value) && type != String.class) {
                result = type;
            } else if (value instanceof String || value instanceof Number) {
                if (type == String.class) {
                    return type;
                } else if (NumberUtils.isNumber(value.toString()) || value instanceof Number) {
                    result = CommonUtils.parseParameterValue(type, value.toString()).getClass();
                    if (type.isPrimitive()) {
                        if ((type == byte.class && (result == byte.class || result == Byte.class))
                                || (type == short.class && (result == byte.class || result == short.class || result == Byte.class || result == Short.class))
                                || (type == int.class && (result == byte.class || result == short.class || result == int.class || result == Byte.class || result == Short.class || result == Integer.class))
                                || (type == float.class && (result == byte.class || result == short.class || result == int.class || result == float.class || result == Byte.class || result == Short.class || result == Integer.class || result == Float.class))
                                || (type == double.class && (result == byte.class || result == short.class || result == int.class || result == float.class || result == double.class || result == Byte.class || result == Short.class || result == Integer.class || result == Float.class || result == Double.class))) {
                            result = type;
                        }
                    } else {
                        result = Number.class.isAssignableFrom(type) ? type : Number.class;
                    }
                } else {
                    result = String.class;
                }
            } else {
                try {
                    result = type.isAssignableFrom(value.getClass()) ? type : value.getClass();
                } catch (NullPointerException e) {
                    result = type;
                }
            }
        }
        return result;
    }

    public static Method findMethod(Class controller, Object[] signal) throws InvalidControllerMethodCallException {
        if (!(signal[0] instanceof String)) {
            throw new InvalidControllerMethodCallException("Signal index[0] not a String");
        }
        Method result = null;
        Method methods[] = controller.getMethods();
        System.out.print("Searching " + controller.getName() + "." + signal[0] + "()... ");
        if (methods.length > 0) {
            int sigCount = signal.length - 1;
            for (Method method : methods) {
                Class[] type = method.getParameterTypes();
                if (method.getName().equalsIgnoreCase(signal[0].toString()) && type.length == sigCount) {
                    result = method;
                    break;
                }
            }
        }
        logger.debug((result == null ? "Not " : "") + "Found!");
        return result;
    }

    public static void main(String[] args) {
        Class cs[] = new Class[]{
            int.class, String.class, Double.class
        };
        Object params[] = new Object[]{
            null, 123456, "Amaran Sidhiq", 123

        };
        params = MethodScanner.getCompatibleParam(cs, params);
        logger.debug(params != null ? "OK" : "NOK");
    }

}
