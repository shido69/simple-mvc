/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import org.apache.log4j.Logger;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Shido69
 */
public abstract class Configuration {

    public abstract void init(ServletConfig config);

    protected void setService(String name, Class clazz) {
        setProperty(name, clazz.getName());
    }

    private void appendJDBCSetting(String connectionName) {
        if (connectionName.isEmpty()) {
            return;
        }
        String strSettings = Env.getConfig().getProperty(Env.JDBC_SETTINGS, "");
        String settings[] = strSettings.split(",");
        boolean valid = true;
        for (String setting : settings) {
            if (setting.trim().equalsIgnoreCase(connectionName)) {
                valid = false;
                break;
            }
        }
        if (valid) {
            strSettings += (strSettings.isEmpty() ? "" : ",") + connectionName;
            setProperty(Env.JDBC_SETTINGS, strSettings);
        }

    }

    public void addJDBCConnection(String driverName, String userName, String password, String url) {
        addJDBCConnection("", driverName, userName, password, url);
    }

    public void addJDBCConnection(String connectionName, String driverName, String userName, String password, String url) {
        if (connectionName == null) {
            return;
        }
        if (!connectionName.isEmpty()) {
            appendJDBCSetting(connectionName);
            setProperty("jdbc.connection." + connectionName + ".user", userName);
            setProperty("jdbc.connection." + connectionName + ".password", password);
            setProperty("jdbc.connection." + connectionName + ".driver", driverName);
//            setProperty("jdbc.connection." + connectionName + ".schema", schema);
            setProperty("jdbc.connection." + connectionName + ".url", url);
        } else {
            setProperty("jdbc.connection.user", userName);
            setProperty("jdbc.connection.password", password);
            setProperty("jdbc.connection.driver", driverName);
//            setProperty("jdbc.connection.schema", schema);
            setProperty("jdbc.connection.url", url);
        }
    }

    protected void setProperty(String key, String value) {
        Env.getConfig().setProperty(key, value);
    }

    public void setControllerLocation(String packageName) {
        setProperty(Env.CONFIG_CONTROLLER_PACKAGE, packageName);
    }

    public void setDefaultUploadLocation(String location) {
        setProperty(Env.FILE_UPLOAD_LOCATION, location);
    }

    public void setViewLocation(String location) {
        setProperty(Env.CONFIG_VIEW_LOCATION, location);
    }

    public Configuration() {
        setProperty("mvc.only_use_new_method", "true");
    }

    private void appendTask(String name) {
        String task = Env.getConfig().getProperty(Env.SCHEDULER_TASK, "");
        if (!task.contains(name)) {
            task += (task.isEmpty() ? "" : ",") + name;
            setProperty(Env.SCHEDULER_TASK, task);
        }
    }

    protected void setTaskCheckingInterval(Integer interval) {
        setProperty("sfx.scheduler.interval", interval.toString());
    }

    private void setTask(String name, String taskType, String time, String className) {
        appendTask(name);
        setProperty(name + ".type", taskType);
        setProperty(name + "." + taskType, time);
        setProperty(name + "." + taskType + ".class", className);
    }

    protected void setDailyTask(String name, String time, Class clazz) {
        if (!Runnable.class.isAssignableFrom(clazz)) {
            throw new RuntimeException("Class does not implement Runnable interface!");
        }
        setTask(name, "daily", time, clazz.getName());
    }

    protected void setWeeklyTask(String name, int dayOfWeek, String time, Class clazz) {
        if (!Runnable.class.isAssignableFrom(clazz)) {
            throw new RuntimeException("Class does not implement Runnable interface!");
        }
        setTask(name, "weekly", dayOfWeek + "," + time, clazz.getName());
    }

    protected void setMonthlyTask(String name, int dayOfMonth, String time, Class clazz) {
        if (!Runnable.class.isAssignableFrom(clazz)) {
            throw new RuntimeException("Class does not implement Runnable interface!");
        }
        setTask(name, "monthly", dayOfMonth + "," + time, clazz.getName());
    }
    private static Logger logger = Logger.getLogger(Configuration.class);

    protected boolean isSessionValid() {
        return Env.getCurrentActiveUser() != null;
    }
}
