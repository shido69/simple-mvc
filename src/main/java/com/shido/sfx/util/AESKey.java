/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;

/**
 *
 * @author Amaran Sidhiq
 */
public class AESKey {
    private static Logger logger= Logger.getLogger(AESKey.class);
    AlgorithmParameterSpec ivspec;
    KeyGenerator keygen;
    Key key;

    public AlgorithmParameterSpec getIvspec() {
        return ivspec;
    }

    public void setIvspec(AlgorithmParameterSpec ivspec) {
        this.ivspec = ivspec;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public KeyGenerator getKeygen() {
        return keygen;
    }

    public void setKeygen(KeyGenerator keygen) {
        this.keygen = keygen;
    }
    String iv = "fedcba9876543210";

    public AESKey(String initialVector, String theKey) throws NoSuchAlgorithmException {
        generateKey(initialVector, theKey);
    }

    public AESKey(String key) throws NoSuchAlgorithmException {
        generateKey(null, key);
    }

    public AESKey() throws NoSuchAlgorithmException {
        generateKey(null, null);
    }

    private void generateKey(String initialVector, String theKey) throws NoSuchAlgorithmException {
        if (initialVector != null) {
            iv = initialVector;
        }
        ivspec = new IvParameterSpec(iv.getBytes());
        if (theKey == null) {
            keygen = KeyGenerator.getInstance("AES");
            keygen.init(128);
            key = keygen.generateKey();
            logger.debug(key.getEncoded().length);
            key = new SecretKeySpec(key.getEncoded(), "AES");
        } else {
            byte[] bkey = theKey.getBytes();
            key = new SecretKeySpec(bkey, "AES");
        }
    }
    
}
