/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Shido69
 */
public class HttpUtils {

    private static Logger logger = Logger.getLogger(HttpUtils.class);

    public static boolean authenticate(String auth, String user, String password) throws IOException {

        if (auth == null) {
            return false;  // no auth
        }
        if (!auth.toUpperCase().startsWith("BASIC ")) {
            return false;  // we only do BASIC
        }
        // Get encoded user and password, comes after "BASIC "
        String userpassEncoded = auth.substring(6);
        // Decode it, using any base 64 decoder
        String userpassDecoded = new String(Base64.getMimeDecoder().decode(userpassEncoded)); //java8 java.util.Base64 implementation

        logger.info("decoded user:password = " + userpassDecoded);
        if (userpassDecoded.equals(user + ":" + password)) {
            return true;
        } else {
            return false;
        }
    }

    private static void printCertificate(HttpsURLConnection http) {
        if (http != null) {

            try {

                logger.debug("Response Code : " + http.getResponseCode());
                logger.debug("Cipher Suite : " + http.getCipherSuite());
                logger.debug("\n");

                Certificate[] certs = http.getServerCertificates();
                for (Certificate cert : certs) {
                    logger.debug("Cert Type : " + cert.getType());
                    logger.debug("Cert Hash Code : " + cert.hashCode());
                    logger.debug("Cert Public Key Algorithm : "
                            + cert.getPublicKey().getAlgorithm());
                    logger.debug("Cert Public Key Format : "
                            + cert.getPublicKey().getFormat());
                    logger.debug("\n");
                }

            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static class DOSSLSocketFactory extends javax.net.ssl.SSLSocketFactory {

        private SSLSocketFactory sf = null;
        private String[] enabledCiphers = null;

        private DOSSLSocketFactory(SSLSocketFactory sf, String[] enabledCiphers) {
            super();
            this.sf = sf;
            this.enabledCiphers = enabledCiphers;
        }

        private Socket getSocketWithEnabledCiphers(Socket socket) {
            if (enabledCiphers != null && socket != null && socket instanceof SSLSocket) {
                ((SSLSocket) socket).setEnabledCipherSuites(enabledCiphers);
            }

            return socket;
        }

        @Override
        public Socket createSocket(Socket s, String host, int port,
                boolean autoClose) throws IOException {
            return getSocketWithEnabledCiphers(sf.createSocket(s, host, port, autoClose));
        }

        @Override
        public String[] getDefaultCipherSuites() {
            return sf.getDefaultCipherSuites();
        }

        @Override
        public String[] getSupportedCipherSuites() {
            if (enabledCiphers == null) {
                return sf.getSupportedCipherSuites();
            } else {
                return enabledCiphers;
            }
        }

        @Override
        public Socket createSocket(String host, int port) throws IOException,
                UnknownHostException {
            return getSocketWithEnabledCiphers(sf.createSocket(host, port));
        }

        @Override
        public Socket createSocket(InetAddress address, int port)
                throws IOException {
            return getSocketWithEnabledCiphers(sf.createSocket(address, port));
        }

        @Override
        public Socket createSocket(String host, int port, InetAddress localAddress,
                int localPort) throws IOException, UnknownHostException {
            return getSocketWithEnabledCiphers(sf.createSocket(host, port, localAddress, localPort));
        }

        @Override
        public Socket createSocket(InetAddress address, int port,
                InetAddress localaddress, int localport) throws IOException {
            return getSocketWithEnabledCiphers(sf.createSocket(address, port, localaddress, localport));
        }

    }

    private static void doTrustToCertificates() throws KeyManagementException, NoSuchAlgorithmException {
        String[] exludedCipherSuites = {"_DHE_", "_DH_"};
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };

        SSLContext context = SSLContext.getInstance("SSL");
        context.init(null, trustAllCerts, new SecureRandom());
        SSLParameters params = context.getSupportedSSLParameters();
        List<String> enabledCiphers = new ArrayList<String>();
        for (String cipher : params.getCipherSuites()) {
            boolean exclude = false;
            if (exludedCipherSuites != null) {
                for (int i = 0; i < exludedCipherSuites.length && !exclude; i++) {
                    exclude = cipher.indexOf(exludedCipherSuites[i]) >= 0;
                }
            }
            if (!exclude) {
                enabledCiphers.add(cipher);
            }
        }
        String[] cArray = new String[enabledCiphers.size()];
        enabledCiphers.toArray(cArray);
        SSLSocketFactory sf = context.getSocketFactory();
        sf = new DOSSLSocketFactory(sf, cArray);
        HttpsURLConnection.setDefaultSSLSocketFactory(sf);
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String urlHostName, SSLSession session) {
                if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
                    logger.debug("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
                }
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

    public static void setBasicHttpPassword(HttpURLConnection http, String user, String password) {
        // String basicAuth = "Basic " + Base64.getEncoder().encodeToString(userPass.getBytes()); //Java8 java.util.Base64
        // String basicAuth = "Basic " + Base64.getEncoder().encodeToString(userPass.getBytes());
        String userPass = user + ":" + password;
        logger.info("using auth:" + userPass);
//        BASE64Encoder base64 = new BASE64Encoder(); //java6
//        String basicAuth = "Basic " + base64.encode(userPass.getBytes());

        String basicAuth = "Basic " + Base64.getEncoder().encodeToString(userPass.getBytes());
        http.setRequestProperty("Authorization", basicAuth);
    }

    public static String execute(HttpURLConnection http) throws IOException {
        final StringBuilder responseText = new StringBuilder();
        execute(http, (String response) -> {
            responseText.append(response);
        });
        return responseText.toString();
    }

    public static void execute(HttpURLConnection http, ResponseListener listener) throws IOException {

        InputStream in = null;
        try {
            in = http.getInputStream();
        } catch (IOException e) {
            if (http != null) {
                in = http.getErrorStream();
            }
        }
        logger.info("Response: HTTP " + http.getResponseCode() + "/ " + http.getResponseMessage());
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            String temp = null;
            while ((temp = reader.readLine()) != null) {
                logger.debug(temp);
                listener.receive(temp);
            }
        } finally {
            reader.close();
        }
    }

    public static HttpURLConnection createURLConnection(String remote) throws IOException, KeyManagementException, NoSuchAlgorithmException {
        String callback = remote;
        boolean useSSL = useSSL = callback.startsWith("https");
        URL url;
        HttpURLConnection http = null;

        url = new URL(callback);
        if (useSSL) {
            doTrustToCertificates();
            http = (HttpsURLConnection) url.openConnection();

        } else {
            http = (HttpURLConnection) url.openConnection();
        }

        CookieManager cookieManager = (CookieManager) CookieHandler.getDefault();
        if (cookieManager != null) {
            if (cookieManager.getCookieStore().getCookies().size() > 0) {
                List<HttpCookie> cookies = cookieManager.getCookieStore().getCookies();
                if (cookies != null) {
                    for (HttpCookie cookie : cookies) {
                        http.setRequestProperty("Cookie", cookie.getName() + "=" + cookie.getValue());
                    }
                }
            }
        }

        if (callback.indexOf("@") > 0) {
            String userPass = callback.substring((useSSL ? "https://" : "http://").length(), callback.indexOf("@"));
            String user = userPass.substring(0, userPass.indexOf(":"));
            String pass = userPass.substring(userPass.indexOf(":") + 1);
            setBasicHttpPassword(http, user, pass);
        }
        return http;
    }

    public static String getIpAddress() throws IOException, KeyManagementException, NoSuchAlgorithmException {
        HttpURLConnection http = createURLConnection("http://myjsonip.appspot.com/");
        String result = execute(http);
        String ip = JSONObject.fromObject(result).getString("ip");
        http.disconnect();
        return ip;
    }

    public static void setData(HttpURLConnection http, String method, String data) throws IOException {
        setData(http, method, data, "application/x-www-form-urlencoded");
    }

    public static void setData(HttpURLConnection http, String method, String data, String contentType) throws IOException {
        if (http != null) {
            http.setRequestMethod(method.toUpperCase());
            http.setRequestProperty("Content-Type", contentType);
            http.setDoOutput(true);
            DataOutputStream post = null;
            try {
                post = new DataOutputStream(http.getOutputStream());
                post.writeBytes(data);
                post.flush();
            } finally {
                if (post != null) {
                    post.close();
                }
            }
        }
    }

    public static void main(String[] args) {
        String result = "";
        try {
            java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
            String ip = getIpAddress();

            String bcaLoginPage = "https://m.klikbca.com/login.jsp";
            String bcaLoginAction = "https://m.klikbca.com/authentication.do";
            String bcaLogoutAction = "https://m.klikbca.com/authentication.do?value(actions)=logout";
            String username = "amaransi1108";
            String password = "005531";

            String data = CommonUtils.join("&", new String[]{
                "value(user_id)=" + username,
                "value(pswd)=" + password,
                "value(Submit)=LOGIN",
                "value(actions)=login",
                "value(user_ip)=" + ip,
                "user_ip=" + ip,
                "value(mobile)=true",
                "mobile=true"}
            );
            logger.debug("Login Using:" + data);

            Connection bcaConnection;
            //Enter klikbca
            bcaConnection = Jsoup.connect(bcaLoginPage);
            result = bcaConnection.execute().body();
            logger.debug("Connected: " + (result != null));

            //Login bca
            bcaConnection = Jsoup.connect(bcaLoginAction);
            bcaConnection.header("REFERER", bcaLoginPage);
            bcaConnection.method(Connection.Method.POST);
            bcaConnection.data(data);
            logger.debug("Logging in...");
            result = bcaConnection.execute().body();
            logger.debug("Logged in: " + (result != null));

            //cek saldo;
            String bcaAccStatment = "https://m.klikbca.com/accountstmt.do?value(actions)=menu";
            bcaConnection = Jsoup.connect(bcaAccStatment);
            bcaConnection.header("REFERER", bcaLoginAction);
            logger.debug("Clicking Menu: Informasi Rekening");
            result = bcaConnection.execute().body();
            logger.debug(result);

            String bcaCheckBalance = "https://m.klikbca.com/balanceinquiry.do";
            bcaConnection = Jsoup.connect(bcaCheckBalance);
            bcaConnection.header("REFERER", bcaAccStatment);
            logger.debug("Clicking Menu: Informasi Saldo");

            logger.debug("Parsing: Page");

            Document doc = bcaConnection.execute().parse();
            Element pagebody = doc.selectFirst("pagebody");

            Elements tables = pagebody.getElementsByTag("table");
            Element table = tables.get(3);
            table.text();
            //logout
            bcaConnection = Jsoup.connect(bcaLogoutAction);
            bcaConnection.header("REFERER", bcaLoginPage);
            logger.debug("Logging out...");
            result = bcaConnection.execute().body();
            logger.debug("Logged out: " + (result != null));

        } catch (IOException | KeyManagementException | NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            logger.error(ex);
        }

    }

    public static void mainx(String[] args) {
        String result = "";
        try {
            java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
            String ip = getIpAddress();

            String bcaLoginPage = "https://m.klikbca.com/login.jsp";
            String bcaLoginAction = "https://m.klikbca.com/authentication.do";
            String bcaLogoutAction = "https://m.klikbca.com/authentication.do?value(actions)=logout";
            String username = "amaransi1108";
            String password = "005531";

            String data = CommonUtils.join("&", new String[]{
                "value(user_id)=" + username,
                "value(pswd)=" + password,
                "value(Submit)=LOGIN",
                "value(actions)=login",
                "value(user_ip)=" + ip,
                "user_ip=" + ip,
                "value(mobile)=true",
                "mobile=true"}
            );
            logger.debug("Login Using:" + data);

            //cookies
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            CookieHandler.setDefault(cookieManager);

            //Enter klikbca
            HttpURLConnection bcaConnection = createURLConnection(bcaLoginPage);
            result = execute(bcaConnection);
            logger.debug("Connected: " + (result != null));

            //Login bca
            bcaConnection = createURLConnection(bcaLoginAction);
            bcaConnection.setRequestProperty("REFERER", bcaLoginPage);
            setData(bcaConnection, "post", data);
            logger.debug("Logging in...");
            result = execute(bcaConnection);
            logger.debug("Logged in: " + (result != null));

            //cek saldo;
            String bcaAccStatment = "https://m.klikbca.com/accountstmt.do?value(actions)=menu";
            bcaConnection = createURLConnection(bcaAccStatment);
            bcaConnection.setRequestProperty("REFERER", bcaLoginAction);
            logger.debug("Clicking Menu: Informasi Rekening");
            result = execute(bcaConnection);
            logger.debug(result);

            String bcaCheckBalance = "https://m.klikbca.com/balanceinquiry.do";
            bcaConnection = createURLConnection(bcaCheckBalance);
            bcaConnection.setRequestProperty("REFERER", bcaAccStatment);
            logger.debug("Clicking Menu: Informasi Saldo");
            result = execute(bcaConnection);

            logger.debug("Parsing: Page");

//            Document doc = Jsoup.parse(result);
//            Element pagebody = doc.selectFirst("pagebody");
//
//            Elements tables = pagebody.getElementsByTag("table");
//            Element table = tables.get(3);
//            table.text();
            //logout
            bcaConnection = createURLConnection(bcaLogoutAction);
            bcaConnection.setRequestProperty("REFERER", bcaLoginPage);
            logger.debug("Logging out...");
            result = execute(bcaConnection);
            logger.debug("Logged out: " + (result != null));
            bcaConnection.disconnect();

        } catch (IOException | KeyManagementException | NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
    }

    public static interface ResponseListener {

        public void receive(String response);
    }
}
