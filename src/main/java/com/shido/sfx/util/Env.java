/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import com.shido.sfx.ApplicationContext;
import com.shido.sfx.ApplicationContextImpl;
import com.shido.sfx.FormRequests;
import com.shido.sfx.Scheduler;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public final class Env {

    private static Map cache;
    public static final String SCHEDULER_TASK = "sfx.scheduler.task";
    public static final String SFX_SCHEDULER_OBJECT = "sfx.scheduler.timer.object";
    public static final String JDBC_SETTINGS = "jdbc.connections.settings";
    private static String BASE_URI = "/";

    public static void reloadSheduledTask() {
        Scheduler ss = (Scheduler) cache.get(SFX_SCHEDULER_OBJECT);
        try {
            ss.reloadSchedule();
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public static Map getCache() {
        if (cache == null) {
            cache = new HashMap();
        }
        return cache;
    }
    private static Properties config;
    public static final String FILE_UPLOAD_LOCATION = "mvc.file.upload.location";
    public static final String MAX_FILE_SIZE = "mvc.file.upload.max.size";
    public static final String FILE_SIZE_THRESHOLD = "mvc.file.threshold.size";

    public static final String DATABASE_SETTING_PARAM = "jdbc.connection.settings";
    public static final String DATABASE_URL_PARAM = "jdbc.connection%s.url";
    public static final String DATABASE_USER_PARAM = "jdbc.connection%s.user";
    public static final String DATABASE_PASSWORD_PARAM = "jdbc.connection%s.password";
    public static final String DATABASE_DRIVER_PARAM = "jdbc.connection%s.driver";
    public static final String DEFAULT_CONTROLLER_PATH = "mvc.controller.default_path";

    public static final String CONFIG_CONTROLLER_PACKAGE = "mvc.controller.package.location";
    public static final String CONFIG_VIEW_LOCATION = "mvc.view.location";

    public static final int APPLICATION_SCOPE = 0;
    public static final int REQUEST_SCOPE = 1;
    public static final int SESSION_SCOPE = 2;

    public static Properties getConfig() {
        return config;
    }

    public static void setConfig(Properties config) {
        Env.config = config;
    }

    public static String getBaseURI() {
        return BASE_URI;
    }

    public static String getBaseURI(String path) {
        return BASE_URI + path;
    }
    private static FormRequests formRequest;

    private static final Logger logger = Logger.getLogger(Env.class);

    public static void closeResults(ResultSet res) {
        logger.debug("Closing Results");
        try {
            res.close();
        } catch (NullPointerException e) {
        } catch (SQLException e) {
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public static void closeStatement(Statement stat) {
        logger.debug("Closing Statement");
        try {
            stat.close();
        } catch (NullPointerException e) {
        } catch (SQLException e) {
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public static void commitTransaction() throws SQLException {
        Connection con = (Connection) getContext().get(ApplicationContext.JDBC_CONNECTION);
        commitTransaction(con);
    }

    public static void commitTransaction(Connection con) throws SQLException {
        if (!isAutoCommit() && con != null) {
            con.commit();
            con.close();
            getContext().remove(ApplicationContext.JDBC_CONNECTION);
            getContext().remove(ApplicationContext.AUTO_COMMIT_CONNECTION);
        }
    }

    public static void rollbackTransaction() throws SQLException {
        Connection con = (Connection) getContext().get(ApplicationContext.JDBC_CONNECTION);
        rollbackTransaction(con);
    }

    public static void rollbackTransaction(Connection con) throws SQLException {
        if (!isAutoCommit() && con != null) {
            con.rollback();
            con.close();
            getContext().remove(ApplicationContext.JDBC_CONNECTION);
            getContext().remove(ApplicationContext.AUTO_COMMIT_CONNECTION);
        }
    }

    public static void closeConnection(Connection con) {
        logger.debug("Closing Connection");
        try {
            if (isAutoCommit()) {
                con.close();
                logger.debug("Closed");
            } else {
                logger.debug("Connection not closed: transaction exist!");
            }
        } catch (NullPointerException e) {
            logger.debug("Line " + e.getStackTrace()[0].getLineNumber() + ":" + e);
        } catch (SQLException e) {
            logger.debug("Line " + e.getStackTrace()[0].getLineNumber() + ":" + e);
        } catch (Exception e) {
            logger.debug("Line " + e.getStackTrace()[0].getLineNumber() + ":" + e);
        }

    }
    private static final Map<String, ApplicationContext> applicationContext = new HashMap();

    private static String getContextId() {
        Long tid;
        logger.debug("Thread Id:" + (tid = Thread.currentThread().getId()));
        return Long.toHexString(tid);
    }

    public static void setContext(ApplicationContext context) {
        String id = getContextId();
        context.put("id", id);
//        logger.debug("set context with id:" + id);
        applicationContext.put(id, context);
    }

    public static ApplicationContext getContext() {
        String id = getContextId();
//        logger.debug("get context with id:" + id);
        return applicationContext.get(id);
    }

    public static void clearContext() {
        String id = getContextId();
        logger.debug("clear context with id:" + id);
        applicationContext.get(id).clear();
        applicationContext.remove(id);
    }
    private static final String MVC_ROUTER = "mvc.router";
    private static final String CONFIG_ROUTER_PARAMETER = "router_config_file";

    public static void init(HttpServletRequest request, HttpServletResponse response, ServletConfig config) {
        ApplicationContext context = new ApplicationContextImpl();
        Env.setContext(context);

        context.put(ApplicationContext.HTTP_REQUEST, request);
        context.put(ApplicationContext.SERVLET_CONFIG, config);
        context.put(ApplicationContext.HTTP_RESPONSE, response);
        context.put(ApplicationContext.SERVLET_CONTEXT, request.getServletContext());

        String detectedMvcCallPath = "";
        if (request.getDispatcherType().compareTo(DispatcherType.REQUEST) == 0) {
            detectedMvcCallPath = request.getPathInfo();
            Env.getContext().put(ApplicationContext.REQUEST_URI, request.getRequestURI());
            Env.getContext().put(ApplicationContext.CONTEXT_PATH, request.getServletContext().getContextPath());
            Env.getContext().put(ApplicationContext.SERVLET_PATH, request.getServletPath());

            if (detectedMvcCallPath == null) {
                detectedMvcCallPath = request.getServletPath();
                detectedMvcCallPath = detectedMvcCallPath.substring(0, detectedMvcCallPath.lastIndexOf('.'));
            }
        } else {
            logger.info("Non Request Dispatch type");
            if (request.getDispatcherType().compareTo(DispatcherType.INCLUDE) == 0) {
                detectedMvcCallPath = (String) request.getAttribute(RequestDispatcher.INCLUDE_PATH_INFO);
                Env.getContext().put(ApplicationContext.REQUEST_URI, (String) request.getAttribute(RequestDispatcher.INCLUDE_REQUEST_URI));
                Env.getContext().put(ApplicationContext.CONTEXT_PATH, (String) request.getAttribute(RequestDispatcher.INCLUDE_CONTEXT_PATH));
                Env.getContext().put(ApplicationContext.SERVLET_PATH, (String) request.getAttribute(RequestDispatcher.INCLUDE_SERVLET_PATH));

                if (detectedMvcCallPath == null) {
                    detectedMvcCallPath = (String) request.getAttribute(RequestDispatcher.INCLUDE_SERVLET_PATH);
                    detectedMvcCallPath = detectedMvcCallPath.substring(0, detectedMvcCallPath.lastIndexOf('.'));
                }
            } else if (request.getDispatcherType().compareTo(DispatcherType.FORWARD) == 0) {
                detectedMvcCallPath = (String) request.getAttribute(RequestDispatcher.FORWARD_PATH_INFO).toString();
                Env.getContext().put(ApplicationContext.REQUEST_URI, (String) request.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI));
                Env.getContext().put(ApplicationContext.CONTEXT_PATH, (String) request.getAttribute(RequestDispatcher.FORWARD_CONTEXT_PATH));
                Env.getContext().put(ApplicationContext.SERVLET_PATH, (String) request.getAttribute(RequestDispatcher.FORWARD_SERVLET_PATH));

                if (detectedMvcCallPath == null) {
                    detectedMvcCallPath = (String) request.getAttribute(RequestDispatcher.FORWARD_SERVLET_PATH);
                    detectedMvcCallPath = detectedMvcCallPath.substring(0, detectedMvcCallPath.lastIndexOf('.'));
                }
            }
        }
        if (detectedMvcCallPath.lastIndexOf(".") > 0) {
            detectedMvcCallPath = detectedMvcCallPath.substring(0, detectedMvcCallPath.lastIndexOf("."));
        }
        logger.info("Request URI: " + Env.getContext().getRequestURI());
        logger.info("Servlet Path: " + Env.getContext().getServletPath());
        logger.info("Context Path: " + Env.getContext().getContextPath());
        String baseURI = Env.getContext().getRequestURI();
        try {
            baseURI = baseURI.substring(0, baseURI.lastIndexOf(request.getServletPath()));
        } catch (StringIndexOutOfBoundsException e) {
            logger.info("baseURL probing fail, fall back to:" + (baseURI = Env.getContext().getContextPath()));
        }
        logger.info("Framework Base URI: " + baseURI);
        Env.BASE_URI = baseURI;
        request.setAttribute("baseURI", baseURI);
        request.setAttribute("fullBaseURI", getFullBaseURI());
        Env.getContext().put(ApplicationContext.BASE_URI, baseURI);

        logger.info("Path Info: " + detectedMvcCallPath);
        detectedMvcCallPath = Env.getRouter(detectedMvcCallPath);
        Env.getContext().put(MVC_CALL_PATH, detectedMvcCallPath);
    }

    public static String getMvcCallPath() {
        return (String) getContext().get(MVC_CALL_PATH);
    }
    private static final String MVC_CALL_PATH = "callPath";

    public static String getRouter(String pathInfo) {
        String result = pathInfo;
        Properties router = new Properties();
        String routerFile = getContext().getServletConfig().getInitParameter(Env.CONFIG_ROUTER_PARAMETER);
        if (routerFile == null) {
            return null;
        }
        String configLocation = "/WEB-INF/" + routerFile;
        InputStream in = null;
        try {
            in = getContext().getServletContext().getResourceAsStream(configLocation);
            router.load(in);
        } catch (IOException | NullPointerException ex) {
            logger.debug("No routing file or using new routing method!");
            return result;
        }

        String[] routers = router.getProperty(Env.MVC_ROUTER, "").split(",");
        for (String route : routers) {
            String pattern = router.getProperty(Env.MVC_ROUTER + "." + route.trim() + ".pattern");
            logger.debug("pattern:" + pattern);
            String[] piSplit;
            if (pathInfo.trim().startsWith("/")) {
                piSplit = pathInfo.substring(1).split("/");
            } else {
                piSplit = pathInfo.split("/");
            }
            for (int i = 1; i < piSplit.length; i++) {
                pattern = pattern.replace("$" + i, piSplit[i]);
            }
            String key;
            if (pattern.equalsIgnoreCase(pathInfo)) {
                key = Env.MVC_ROUTER + "." + route.trim() + ".route";
                result = router.getProperty(key);
                for (int i = 1; i < piSplit.length; i++) {
                    result = result.replace("$" + i, piSplit[i]);
                }
                logger.info("Router config found! [" + key + " Routing to:" + result + "]");
                break;
            }
        }
        return result;
    }

    private static final String ADDITIONAL_HEADER = "additionalHeader";
    private static final String ADDITIONAL_FOOTER = "additionalFooter";

    public static void setAdditionalHeader(String page) {
        Env.getContext().getHttpServletRequest().setAttribute(Env.ADDITIONAL_HEADER, page);
    }

    public static void setAdditionalFooter(String page) {
        Env.getContext().getHttpServletRequest().setAttribute(Env.ADDITIONAL_FOOTER, page);
    }

    public static void beginTransaction(Connection con) throws SQLException {
        getContext().put(ApplicationContext.AUTO_COMMIT_CONNECTION, Boolean.FALSE);
        con.setAutoCommit(false);
        getContext().put(ApplicationContext.JDBC_CONNECTION, con);
    }

    public static boolean isAutoCommit() {
        boolean result = true;
        try {
            Object o = getContext().get(ApplicationContext.AUTO_COMMIT_CONNECTION);
            if (o != null) {
                result = (Boolean) o;
            }
        } catch (NullPointerException e) {
        }
        return result;
    }

    public static void setCurrentActiveUser(Map user) {
        getContext().setCurrentActiveUser(user);
    }

    public static Map getCurrentActiveUser() {
        return getContext().getCurrentActiveUser();
    }

    public static String getDBSechema(String settings) {
        String setting = (settings == null ? "" : (settings.isEmpty() ? "" : ("." + settings)));
        String url = getConfig().getProperty(String.format(Env.DATABASE_URL_PARAM, setting));
        if (url == null) {
            url = getConfig().getProperty(String.format(Env.DATABASE_URL_PARAM, ""));
            if (url == null) {
                return null;
            }
        }
        if (url.startsWith("jdbc:postgresql")) {
            try {
                String[] params = url.substring(url.indexOf("?") + 1).split("&");
                for (String param : params) {
                    if (param.startsWith("currentSchema")) {
                        return param.substring(param.indexOf("=") + 1);
                    }
                }
            } catch (NullPointerException | ArrayIndexOutOfBoundsException | StringIndexOutOfBoundsException e) {
                logger.warn(e);
            }
        } else {
            logger.warn("default schema 'Not Supported' by this protocol!");
        }

        return null;
    }

    public static void setDebugSQL(boolean b) {
        getContext().put(ApplicationContext.SQL_DEBUG, b);
    }

    public static boolean isDebugSQL() {
        boolean result = false;
        if (getContext() != null) {
            if (getContext().get(ApplicationContext.SQL_DEBUG) != null) {
                result = (Boolean) getContext().get(ApplicationContext.SQL_DEBUG);
            }
        }
        return result;
    }

    public static String getFullBaseURI() {
        return getFullBaseURI("");
    }

    public static String getFullBaseURI(String path) {
        String serverName = Env.getContext().getHttpServletRequest().getServerName();
        int portNumber = Env.getContext().getHttpServletRequest().getServerPort();
        String contextPath = Env.getContext().getHttpServletRequest().getContextPath();
        return (portNumber == 443 ? "https" : "http") + "://" + serverName + (portNumber == 80 || portNumber == 443 ? "" : ":" + portNumber) + contextPath + path;
    }

    public static String decrypt(String key128bit, String string) {
        try {
            AESDecryption decryptor = new AESDecryption(new AESKey(key128bit));
            return decryptor.decrypt(string);
        } catch (Exception e) {
            return string;
        }
    }

    public static String encrypt(String key128bit, String string) {
        try {
            AESEncryption encryptor = new AESEncryption(new AESKey(key128bit));
            return encryptor.encrypt(string);
        } catch (Exception e) {
            return string;
        }
    }

}
