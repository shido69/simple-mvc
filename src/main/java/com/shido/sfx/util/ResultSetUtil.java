/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public class ResultSetUtil {

    public static List<Map> toList(ResultSet resultSet) throws SQLException {
        List<Map> result = new ArrayList();
        boolean next;
        do {
            Map row = ResultSetUtil.toMap(resultSet);
            if (next = row != null) {
                result.add(row);
            }
        } while (next);
        return result;
    }

    public static Map toMap(ResultSet resultSet) throws SQLException {
        Map<String, Object> result = null;
        ResultSetMetaData meta = resultSet.getMetaData();
        if (resultSet.next()) {
            result = new LinkedHashMap();
            int colCount = meta.getColumnCount();
            for (int i = 0; i < colCount; i++) {
                result.put(meta.getColumnLabel(i + 1), resultSet.getObject(i + 1));
            }
        }
        return result;
    }
}
