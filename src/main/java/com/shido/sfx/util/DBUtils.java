/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import com.shido.sfx.ApplicationContext;
import com.shido.sfx.RowHandler;
import com.shido.sfx.ServiceFactory;
import com.shido.sfx.annotations.Table;
import com.shido.sfx.annotations.Column;
import com.shido.sfx.annotations.PrimaryKey;
import com.shido.sfx.annotations.Join;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class DBUtils {

    public static Object getSingleValue(StringBuilder sql, String key, Object... params) {
        return getSingleValue(sql.toString(), key, params);
    }

    public static Object getSingleValue(String sql, String key, Object... params) {
        Object result = null;
        Map query = getSingle(sql, params);
        if (query != null) {
            result = query.get(key);
        }
        return result;
    }

    private static Object getModelPrimaryKeyValue(Object model) {
        Class<?> modelClass = (Class<?>) model.getClass();
        Object val = null;
        for (Field f : modelClass.getDeclaredFields()) {
            if (f.isAnnotationPresent(PrimaryKey.class)) {
                try {
                    f.setAccessible(true);
                    val = f.get(model);
                } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
                    logger.debug(e);
                }
                break;
            }
        }
        return val;
    }

    public static String generateInsert(Object model) {
        Class<?> modelClass = (Class<?>) model.getClass();
        String tableName = modelClass.getAnnotation(Table.class).value();
        Map values = new HashMap();
        for (Field f : modelClass.getDeclaredFields()) {
            if (f.isAnnotationPresent(Column.class)) {
                try {
                    f.setAccessible(true);
                    String colName = f.getAnnotation(Column.class).value();
                    Object val = null;
                    val = f.get(model);
                    if (val != null) {
                        if (!(val instanceof List) && val.getClass().isAnnotationPresent(Table.class)) {
                            val = getModelPrimaryKeyValue(val);
                        }
                    } else {
                        continue;
                    }
                    values.put(colName, val);
                } catch (IllegalAccessException | IllegalArgumentException e) {
                }
            }
        }
        return generateInsert(tableName, values);
    }

    public static String generateInsert(String tableName, Map values) {
        return generateInsert(tableName, values, false);
    }

    public static String generateInsert(String tableName, Map values, boolean replace) {
        StringBuilder sql = new StringBuilder();
        StringBuilder col = new StringBuilder();
        StringBuilder val = new StringBuilder();

        Set keys = values.keySet();
        for (Iterator<String> keyIterator = keys.iterator(); keyIterator.hasNext();) {
            String key = keyIterator.next();
            col.append("\"").append(key.toLowerCase()).append("\"");
            Object value = values.get(key);
            if (value == null) {
                val.append("null");
            } else {
                if (value instanceof Number) {
                    val.append(((Number) value).toString());
                } else {
                    if (value instanceof String) {
                        if (((String) value).equals("?")) {
                            val.append("?");
                        } else {
                            val.append("'").append(value.toString().replace("'", "''")).append("'");
                        }
                    } else {
                        val.append("'");
                        if (value instanceof Timestamp) {
                            val.append(String.format("%1$tF %1$tT", (Date) value));
                        } else if (value instanceof Time) {
                            val.append(String.format("%tT", (Date) value));
                        } else if (value instanceof Date || value instanceof Calendar) {
                            val.append(String.format("%tF", value instanceof Calendar ? ((Calendar) value).getTime() : (Date) value));
                        }
                        val.append("'");
                    }
                }
            }

            if (keyIterator.hasNext()) {
                col.append(", ");
                val.append(", ");
            }
        }
        if (replace) {
            sql.append("Replace into ");
        } else {
            sql.append("Insert into ");
        }
        sql.append(tableName);
        sql.append(" (");
        sql.append(col);
        sql.append(") values(");
        sql.append(val);
        sql.append(")");
        return sql.toString();
    }

    public static String generateUpdate(String tableName, Map values) {
        return generateUpdate(tableName, values, tableName + "_id");
    }

    public static String generateUpdate(String tableName, Map values, String keyFieldName) {
        StringBuilder sql = new StringBuilder();
        StringBuilder sets = new StringBuilder();

        String filter = keyFieldName + "=?";
        Set keys = values.keySet();
        filter = keys.contains(keyFieldName) ? filter : null;
        for (Iterator<String> keyIterator = keys.iterator(); keyIterator.hasNext();) {
            String key = keyIterator.next();
            if (key.equalsIgnoreCase(keyFieldName)) {
                continue;
            }
            sets.append(", ");
            sets.append(key).append("=");
            Object value = values.get(key);
            if (value == null) {
                sets.append("null");
            } else {
                if (value instanceof Number) {
                    sets.append(((Number) value).toString());
                } else {
                    sets.append("'");
                    if (value instanceof Timestamp) {
                        sets.append(String.format("%1$tF %1$tT", (Date) value));
                    } else if (value instanceof Time) {
                        sets.append(String.format("%tT", (Date) value));
                    } else if (value instanceof Date || value instanceof Calendar) {
                        sets.append(String.format("%tF", (Date) (value instanceof Calendar ? ((Calendar) value).getTime() : (Date) value)));
                    } else {
                        value = value.toString().replace("'", "''");
                        sets.append(value);
                    }
                    sets.append("'");
                }
            }
        }
        sql.append("Update ");
        sql.append(tableName);
        sql.append(" Set ");
        sql.append(sets.substring(1));
        if (filter != null) {
            sql.append(" where ");
            sql.append(filter);
        }
        return sql.toString();
    }
    private static Logger logger = Logger.getLogger(DBUtils.class.getName());

    public static Connection getConnection() throws SQLException {
        return getConnection("");
    }

    public static Connection getConnection(String setting) throws SQLException {
        Connection con = null;
        if (Env.isAutoCommit()) {
            con = ServiceFactory.getDataSource(setting).getConnection();
        } else {
            con = (Connection) Env.getContext().get(ApplicationContext.JDBC_CONNECTION);
        }
        return con;
    }

    public static int executeNonQuery(String sql, boolean returnKey, Object... params) throws SQLException {
        Connection con = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        int result = 0;
        try {
            con = getConnection();
            stat = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            if (params != null) {
                int idx = 1;
                for (Object p : params) {
                    if (p instanceof File) {
                        FileInputStream fis = new FileInputStream((File) p);
                        stat.setBinaryStream(idx++, fis, ((File) p).length());
                    } else if (p instanceof byte[]) {
                        stat.setBytes(idx++, (byte[]) p);
                    } else {
                        stat.setObject(idx++, p);
                    }
                }
            }
            if (Env.isDebugSQL()) {
                logger.info(stat);
            }
            result = stat.executeUpdate();
            if (returnKey) {
                if (result > 0) {
                    rs = stat.getGeneratedKeys();
                    if (rs.next()) {
                        result = rs.getInt(1);
                    }
                }
            }
        } catch (Exception e) {
            result = 0;
            logger.error(e);
            if (e instanceof SQLException) {
                throw (SQLException) e;
            }
        } finally {
            Env.closeResults(rs);
            Env.closeStatement(stat);
            Env.closeConnection(con);
        }
        return result;
    }

    public static List getSingleColumnValueList(String sql, String key, Object... params) {
        if (key == null) {
            throw new NullPointerException("Key is null");
        }
        List<Map> query = getList(sql, params);
        List result = null;
        if (query != null) {
            result = new ArrayList();
            for (Map m : query) {
                result.add(m.get(key));
            }
        }
        return result;
    }

    public static List getListWithRowHandler(String sql, RowHandler handler, Object... params) throws SQLException {
        Connection con = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        List result = null;
        try {
            con = getConnection();
            stat = con.prepareStatement(sql);
            if (params != null) {
                int idx = 1;
                for (Object p : params) {
                    stat.setObject(idx++, p);
                }
            }
            if (Env.isDebugSQL()) {
                logger.info("SQL: " + stat);
            }
            res = stat.executeQuery();
            int idx = 0;
            Map row = null;
            while ((row = toMap(res)) != null) {
                if (handler.rowData(idx, row)) {
                    if (result == null) {
                        result = new ArrayList();
                    }
                    result.add(row);
                }
            }

        } finally {
            Env.closeResults(res);
            Env.closeStatement(stat);
            Env.closeConnection(con);
        }
        return result;
    }

    public static Object getQuery(String sql, boolean single, Object... params) throws SQLException {
        Connection con = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        Object result = null;
        try {
            con = getConnection();
            stat = con.prepareStatement(sql);
            if (params != null) {
                int idx = 1;
                for (Object p : params) {
                    stat.setObject(idx++, p);
                }
            }
            if (Env.isDebugSQL()) {
                logger.info("SQL: " + stat);
            }
            res = stat.executeQuery();
            if (!single) {
                result = toList(res);
            } else {
                result = toMap(res);
            }
        } finally {
            Env.closeResults(res);
            Env.closeStatement(stat);
            Env.closeConnection(con);
        }
        return result;
    }

    private static String getModelPKFieldName(Class t) {
        for (Field f : t.getDeclaredFields()) {
//            logger.info("f=" + f.getName());
            if (f.isAnnotationPresent(PrimaryKey.class)) {
                return f.getAnnotation(Column.class).value();
            }
        }
        return null;
    }

    private static <T> T mapToObject(Map src, Class<T> tgt) {
        return mapToObject(src, tgt, true);
    }

    private static <T> T mapToObject(Map src, Class<T> tgt, boolean ignoreJoin) {
        T target = null;
//        Map cache = (Map) Env.getCache().get("orm_cache");
//        if (cache == null) {
//            cache = new HashMap<String, Object>();
//            Env.getCache().put("orm_cache", cache);
//        }
        if (src != null) {
            String key = tgt.getName() + "-" + tgt.getAnnotation(Table.class).value() + "-" + src.get(getModelPKFieldName(tgt));
//            if ((target = (T) cache.get(key)) == null) {
            try {
                target = tgt.newInstance();
//                    cache.put(key, target);
            } catch (IllegalAccessException | InstantiationException e) {
            }
            for (Field f : tgt.getDeclaredFields()) {
                String colName = null;
                Object val = null;
                if (f.isAnnotationPresent(Column.class)) {
                    colName = f.getAnnotation(Column.class).value();
                    val = src.get(colName);
                }
                if (f.isAnnotationPresent(Join.class) && !ignoreJoin) {
                    Class<?> t = f.getType();
                    String pkField = getModelPKFieldName(tgt);
                    String fkField = f.getAnnotation(Join.class).value();

                    if (val == null) {
                        val = src.get(pkField);
                    }

                    if (f.getType() == List.class) {
                        ParameterizedType lstObjType = (ParameterizedType) f.getGenericType();
                        t = (Class<?>) lstObjType.getActualTypeArguments()[0];
                        val = getList(t, "Where " + (fkField.isEmpty() ? pkField : fkField) + "=?", val);
                    } else {
                        try {
                            val = getSingle(t, "Where " + (fkField.isEmpty() ? pkField : fkField) + "=?", val);
                        } catch (SQLException e) {
                            logger.error(e);
                            val = null;
                        }
                    }
                }
                try {
                    f.setAccessible(true);
                    f.set(target, val);
                } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
//                        logger.error(e);
                }
            }
//            }
        }
        return target;

    }

    public static <T> List<T> getListWithJoin(Class<T> target) {
        return getListWithJoin(target, null);
    }

    public static <T> List<T> getListWithJoin(Class<T> target, String additionalQuery, Object... parameter) {
        String sql = "Select * from %s %s";
        List<Map> l = getList(String.format(sql, target.getAnnotation(Table.class).value(), additionalQuery != null ? additionalQuery : ""), parameter);
        List<T> result = null;
        if (l != null) {
            result = new ArrayList();
            for (Map m : l) {
                result.add(mapToObject(m, target, false));
            }
        }
        return result;
    }

    public static <T> List<T> getList(Class<T> target) {
        return getList(target, null);
    }

    private static String getColumn(Class<?> target) {
        String result = "";
        for (Field f : target.getDeclaredFields()) {
            if (f.isAnnotationPresent(Column.class)) {
                result += f.getAnnotation(Column.class).value() + ", ";
            }
        }
        return result.isEmpty() ? "*" : result.substring(0, result.lastIndexOf(","));
    }

    public static <T> List<T> getList(Class<T> target, String additionalQuery, Object... parameter) {
        String column = getColumn(target);
        String sql = "Select %s from %s %s";
        List<Map> l = getList(String.format(sql, column, target.getAnnotation(Table.class).value(), additionalQuery != null ? additionalQuery : ""), parameter);
        List<T> result = null;
        if (l != null) {
            result = new ArrayList();
            for (Map m : l) {
                result.add(mapToObject(m, target, true));
            }
        }
        return result;
    }

    public static List getList(String sql) {
        return getList(sql, (Object[]) null);
    }

    public static List getList(StringBuilder sql, Object... params) {
        return getList(sql, false, params);
    }

    public static List getList(StringBuilder sql, boolean raiseException, Object... params) {
        return getList(sql.toString(), raiseException, params);
    }

    public static List getList(String sql, Object... params) {
        return getList(sql, false, params);
    }

    public static List getList(String sql, boolean raiseException, Object... params) {
        List result = null;
        try {
            result = (List) getQuery(sql, false, params);
        } catch (SQLException ex) {
            logger.error(ex);
            if (raiseException) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    public static <T> T getSingleWithJoin(Class<T> target, Object id) throws SQLException {
        return getSingleWithJoin(target, "Where " + getModelPKFieldName(target) + "=?", id);
    }

    public static <T> T getSingleWithJoin(Class<T> target, String additionalQuery, Object... params) throws SQLException {
        String column = getColumn(target);
        String sql = "Select %s from %s %s";
        Map m = getSingle(String.format(sql, column, target.getAnnotation(Table.class).value(), additionalQuery == null ? "" : additionalQuery), params);
        T result = mapToObject(m, target, false);
        return result;
    }

    public static <T> T getSingle(Class<T> target, Object id) throws SQLException {
        return getSingle(target, "Where " + getModelPKFieldName(target) + "=?", id);
    }

    public static <T> T getSingle(Class<T> target, String additionalQuery, Object... parameter) throws SQLException {
        String sql = "Select * from %s %s";
        Map m = getSingle(String.format(sql, target.getAnnotation(Table.class).value(), additionalQuery != null ? additionalQuery : ""), parameter);
        T result = mapToObject(m, target);
        return result;
    }

    public static Map getSingle(String sql) {
        return getSingle(sql, (Object[]) null);
    }

    public static Map getSingle(String sql, Object... params) {
        return getSingle(sql, false, params);
    }

    public static Map getSingle(String sql, boolean raiseException, Object... params) {
        Map result = null;
        try {
            result = (Map) getQuery(sql, true, params);
        } catch (SQLException ex) {
            logger.error(ex);
            if (raiseException) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    public static List<Map> toList(ResultSet resultSet) throws SQLException {
        List<Map> result = new ArrayList();
        boolean next;
        do {
            Map row = DBUtils.toMap(resultSet);
            if (next = row != null) {
                result.add(row);
            }
        } while (next);
        return result;
    }

    public static class Persistence extends LinkedHashMap<String, Object> {

        public String getValueAsString(String key) {
            return getValueAsString(key, null);
        }

        public String getValueAsString(String key, String defaultVal) {
            Object val = get(key);
            if (val != null) {
                if (val instanceof String) {
                    return (String) val;
                } else if (val instanceof Number) {
                    BigDecimal x = BigDecimal.valueOf(((Number) val).doubleValue());
                    return x.toPlainString();
                } else {
                    return val.toString();
                }
            }
            return defaultVal;
        }

        public int getValueAsInteger(String key) {
            Object val = get(key);
            if (val != null) {
                if (val instanceof Number) {
                    BigDecimal x = BigDecimal.valueOf(((Number) val).doubleValue());
                    return x.intValueExact();
                }
            }

            return 0;
        }

        public long getValueAsLong(String key) {
            Object val = get(key);
            if (val != null) {
                if (val instanceof Number) {
                    BigDecimal x = BigDecimal.valueOf(((Number) val).doubleValue());
                    return x.longValueExact();
                }
            }
            return 0L;
        }

        public double getValueAsDouble(String key) {
            Object val = get(key);
            if (val != null) {
                BigDecimal x;
                if (val instanceof Number) {
                    if (val instanceof BigDecimal) {
                        x = (BigDecimal) val;
                    } else {
                        x = BigDecimal.valueOf(((Number) val).doubleValue());
                    }
                } else if (val instanceof String) {
                    x = new BigDecimal((String) val);
                } else {
                    throw new NumberFormatException();
                }
                return x.doubleValue();
            }
            return 0L;

        }

        public boolean getValueAsBoolean(String key) {
            return getValueAsBoolean(key, null);
        }

        public boolean getValueAsBoolean(String key, String compare) {
            Object val = get(key);
            if (val != null) {
                BigDecimal x;
                if (val instanceof Number) {
                    if (val instanceof BigDecimal) {
                        x = (BigDecimal) val;
                    } else {
                        x = BigDecimal.valueOf(((Number) val).doubleValue());
                    }
                    return x.compareTo(BigDecimal.ZERO) != 0;
                } else if (val instanceof String) {
                    String check = (String) val;
                    if (compare != null) {
                        return check.equalsIgnoreCase(compare);
                    }
                    return check.equalsIgnoreCase("Y")
                            || check.equalsIgnoreCase("Yes")
                            || check.equalsIgnoreCase("True");
                }
            }
            return false;
        }
    }

    public static Map toMap(ResultSet resultSet) throws SQLException {
        Map<String, Object> result = null;
        ResultSetMetaData meta = resultSet.getMetaData();
        if (resultSet.next()) {
            result = new Persistence();
            int colCount = meta.getColumnCount();
            for (int i = 0; i < colCount; i++) {
                result.put(meta.getColumnLabel(i + 1), resultSet.getObject(i + 1));
            }
        }
        return result;
    }

    public static Object parseDBStringObject(String string) {
        if (string == null) {
            return null;
        }
        if (!string.isEmpty()) {
            String sql = String.format("select %s as result", string);
            return getSingleValue(sql, "result");
        }
        return null;
    }

    public static Map getListItemsAsMap(String sql, String key, Object... params) {
        return getListItemsAsMap(sql, key, "*", params);
    }

    public static Map getListItemsAsMap(String sql, String key, String value, Object... params) {
        List<Map> l = getList(sql, params);
        return CommonUtils.listToMap(l, key, value);
    }

    public static void insert(Map data, String tableName) throws SQLException {
        String sql = generateInsert(tableName, data);
        logger.debug(sql);
        DBUtils.executeNonQuery(sql, true);
    }

    public static int update(Map updateMap, String tableName, String key, Object id) throws SQLException {
        if (!updateMap.containsKey(key)) {
            updateMap.put(key, id);
        }
        String sql = generateUpdate(tableName, updateMap, key);
        logger.info(sql);
        return DBUtils.executeNonQuery(sql, false, id);
    }

    public static void clearFieldValue(String field, String tableName, String condition) throws SQLException {
        String sql = "update " + tableName + " set " + field + "=null " + (condition != null ? (condition.isEmpty() ? "" : "where " + condition) : "");
        DBUtils.executeNonQuery(sql, false);
    }

    public static Object[] createCondition(StringBuilder sql, Map<String, Object> otherParams, String conjuction) {
        return createCondition(sql, otherParams, "and", conjuction);
    }

    public static Object[] createCondition(StringBuilder sql, Map<String, Object> otherParams, String conjuction, String conjuction2) {
        Object[] result = null;
        if (otherParams != null) {
            if (otherParams.isEmpty()) {
                return result;
            }
            String conditions = "";
            List lparams = new ArrayList();
            for (Map.Entry<String, Object> entry : otherParams.entrySet()) {
                String condition = entry.getKey();
                condition = condition.trim();
                Object val = entry.getValue();
                if (val == null) {
                    continue;
                }
                String op = condition.endsWith(" in") || condition.endsWith("like") || condition.endsWith(">=") || condition.endsWith("<=")
                        || condition.endsWith(">") || condition.endsWith("<")
                        || condition.endsWith("!=") || condition.endsWith("=") ? " " : " = ";
                if (val instanceof List || val.getClass().isArray()) {
                    op = " in ";
                    if (val instanceof List) {
                        conditions += condition + op + CommonUtils.toInString((List) val);
                    } else {
                        conditions += condition + op + CommonUtils.toInString((Object[]) val);
                    }
                } else {
                    conditions += condition + op + "?";
                    lparams.add(val);
                }
                conditions += " " + conjuction2 + " ";
            }
            if (!sql.toString().contains("where")) {
                sql.append(" where ");
            } else {
                sql.append(" ").append(conjuction).append(" ");
            }
            sql.append("(");
            sql.append(conditions.substring(0, conditions.length() - (lparams.size() > 0 ? conjuction2.length() + 2 : 0)));
            sql.append(")");
            result = lparams.toArray();
            lparams.clear();
        }
        return result;
    }
}
