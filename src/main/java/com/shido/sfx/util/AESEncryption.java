/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Amaran Sidhiq
 */
public class AESEncryption {

    Cipher cipher;

    public Cipher getCipher() {
        return cipher;
    }

    public void setCipher(Cipher cipher) {
        this.cipher = cipher;
    }

    public AESEncryption(AESKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        this(key.getKey(), key.getIvspec());
    }

    public AESEncryption(Key keyspec, AlgorithmParameterSpec ivspect) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspect);
    }

    public String encrypt(String text) throws IllegalBlockSizeException, BadPaddingException {
        byte[] encrypted = cipher.doFinal(CommonUtils.padString(text).getBytes());
        return Base64.getEncoder().encodeToString(encrypted);
    }

    public static void main(String[] args) {
        try {
            AESKey key=new AESKey("empress3m12355--");
            AESEncryption encryptor = new AESEncryption(key);
            String chiped=encryptor.encrypt("Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy Text ini akan di encrypy ");
            System.out.println(chiped);
            AESDecryption decryptor=new AESDecryption(key);
            System.out.println(decryptor.decrypt(chiped));
        } catch (Exception ex) {
            Logger.getLogger(AESEncryption.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
