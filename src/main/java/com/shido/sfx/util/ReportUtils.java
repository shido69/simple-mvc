/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.Graphics2DExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleHtmlReportConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.export.type.HtmlSizeUnitEnum;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;
import net.sf.jasperreports.web.util.WebHtmlResourceHandler;
import org.apache.log4j.Logger;

/**
 *
 * @author lenovo
 */
public class ReportUtils {

    private static Logger logger = Logger.getLogger(ReportUtils.class);

    public static String getReportName(String reportPath) throws JRException {
        File report = new File(reportPath);
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(report);
        String name = jasperReport.getName();
        jasperReport = null;
        return name;
    }

    public static JRParameter[] getReportParameters(String reportPath) throws JRException {
        File report = new File(reportPath);
        logger.info("Report Exist:" + report);
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(report);
        JRParameter[] params = jasperReport.getParameters();
        List list = new ArrayList();
        for (JRParameter param : params) {
            if (!param.isSystemDefined() && param.isForPrompting()) {
                logger.info(param.getName());
                list.add(param);
            }
        }
        JRParameter[] result = (JRParameter[]) list.toArray(new JRParameter[list.size()]);
        list.clear();
        return result;
    }

    public static void jaspertToPdfStream(String reportPath, String subReportPath, Map params, Collection<Map<String, ?>> data, OutputStream out) throws JRException, SQLException {
        InputStream streamReport = JRLoader.getFileInputStream(reportPath);

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }
        reportParameter.put(JRPdfExporterParameter.METADATA_TITLE, getReportName(reportPath));

        if (params != null) {
            reportParameter.putAll(params);
        }
        JRMapCollectionDataSource ds = new JRMapCollectionDataSource(data);
        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, ds);

//        JasperViewer jv = new JasperViewer(report);
//        jv.setTitle("Work Order Management");
//        jv.setVisible(true);
        JasperExportManager.exportReportToPdfStream(report, out);
    }

    public static void jaspertToPdfStream(String reportPath, String subReportPath, Map params, Connection con, OutputStream out) throws JRException, SQLException {
        InputStream streamReport = JRLoader.getFileInputStream(reportPath);

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }
        reportParameter.put(JRPdfExporterParameter.METADATA_TITLE, getReportName(reportPath));

        if (params != null) {
            reportParameter.putAll(params);
        }

        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, con);

//        JasperViewer jv = new JasperViewer(report);
//        jv.setTitle("Work Order Management");
//        jv.setVisible(true);
        JasperExportManager.exportReportToPdfStream(report, out);
    }

    public static void jaspertToPdfFile(String jasperPath, String subReportPath, Map params, Connection con, File out) throws FileNotFoundException, JRException, SQLException {
        jaspertToPdfStream(jasperPath, subReportPath, params, con, new FileOutputStream(out));
    }

    public static void main_1(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Adempiere", "adempiere", "adempiere");
            File out = new File("C:\\Users\\lenovo\\Documents\\personel.pdf");
            if (!out.exists()) {
                out.createNewFile();
            }
            Map params = new HashMap();
            params.put(JRParameter.IS_IGNORE_PAGINATION, false);
            jaspertToPdfFile("C:\\Users\\lenovo\\Documents\\HRD DAKOTA\\personel.jasper", null, params, con, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main2(String[] args) throws JRException {
        logger.debug(getReportName("C:\\Users\\shido\\Documents\\NetBeansProjects\\Empress\\build\\web\\WEB-INF\\jasper\\personel.jasper"));
    }

    public static void main(String[] args) throws FileNotFoundException, DocumentException, DocumentException, IOException {
        File f = new File("C:\\Users\\shido\\Documents\\test.pdf");
        f.getParentFile().mkdir();
        if (!f.exists()) {
            f.createNewFile();
        }
        FileOutputStream file = new FileOutputStream(f);
        List data = new ArrayList();
        Map row = new LinkedHashMap();
        row.put("row_num", 0);
        String uuid = UUID.randomUUID().toString();
        for (int i = 1; i <= 12; i++) {
            row.put("col_" + i, uuid.substring(0, uuid.length() - i));
        }
        for (int i = 0; i < 100; i++) {
            row.put("row_num", i + 1);
            data.add(new LinkedHashMap(row));
        }

        listMapToPDF(data, "A3", true, file);
    }

    public static void listMapToPDF(List<Map> datas, boolean rotate, OutputStream out) throws BadElementException, DocumentException, IOException {
        listMapToPDF(datas, "A4", rotate, out);
    }

    public static void listMapToPDF(List<Map> datas, String paperSize, boolean rotate, OutputStream out) throws BadElementException, DocumentException, IOException {
        Document pdfDoc = new Document();
        PdfWriter.getInstance(pdfDoc, out);
        Rectangle pageSise = PageSize.getRectangle(paperSize);
        if (rotate) {
            pageSise = pageSise.rotate();
        }
        pdfDoc.setPageSize(pageSise);
        pdfDoc.open();
        if (datas == null) {
            return;
        }
        if (datas.isEmpty()) {
            return;
        }

        String[] headers = (String[]) ((Map) datas.get(0)).keySet().toArray(new String[((Map) datas.get(0)).size()]);

        PdfPTable table = new PdfPTable(headers.length);
        table.setSplitLate(false);
        table.setWidthPercentage(100f);
        String colHeader;
        for (String header : headers) {
            colHeader = CommonUtils.cleanFieldName(header);
            table.addCell(colHeader);
        }
        String cellValue;
        for (Map data : datas) {
            for (String header : headers) {
                cellValue = data.get(header) != null ? data.get(header).toString() : "";
                table.addCell(cellValue);
            }
        }
        pdfDoc.add(table);
        pdfDoc.close();
    }

    public static void jaspertToXLSStream(String reportPath, String subReportPath, Map params, Connection con, OutputStream out) throws JRException {
        InputStream streamReport = JRLoader.getFileInputStream(reportPath);

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }

        if (params != null) {
            reportParameter.putAll(params);
        }
        reportParameter.put(JRParameter.IS_IGNORE_PAGINATION, true);

        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, con);
        xlsExporter(report, out);
    }

    private static void xlsExporter(JasperPrint report, OutputStream out) throws JRException {

        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setExporterInput(new SimpleExporterInput(report));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
        SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
        configuration.setOnePagePerSheet(true);
        configuration.setIgnorePageMargins(Boolean.TRUE);
        configuration.setDetectCellType(true);
        configuration.setCollapseRowSpan(false);
        exporter.setConfiguration(configuration);

        exporter.exportReport();
    }

    public static void jaspertToXLSStream(String reportPath, String subReportPath, Map params, Collection<Map<String, ?>> data, OutputStream out) throws JRException {
        InputStream streamReport = JRLoader.getFileInputStream(reportPath);

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }

        if (params != null) {
            reportParameter.putAll(params);
        }
        reportParameter.put(JRParameter.IS_IGNORE_PAGINATION, true);
        JRMapCollectionDataSource ds = new JRMapCollectionDataSource(data);
        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, ds);
        xlsExporter(report, out);
    }

    public static void jasperToHtml(String reportPath, String subReportPath, String imageURI, Map params, Collection<Map<String, ?>> data, OutputStream out) throws JRException {
        File f = new File(reportPath);
        InputStream streamReport = JRLoader.getFileInputStream(f.getAbsolutePath());

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }
//        reportParameter.put(JRPdfExporterParameter.METADATA_TITLE, getReportName(reportPath));

        if (params != null) {
            reportParameter.putAll(params);
        }
        reportParameter.put(JRParameter.IS_IGNORE_PAGINATION, true);
        JRMapCollectionDataSource ds = new JRMapCollectionDataSource(data);
        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, ds);

        htmlExporter(report, imageURI, Env.getContext().getHttpServletRequest(), out);
    }

    public static void jasperToHtml(String reportPath, String subReportPath, String imageURI, Map params, Connection con, OutputStream out) throws JRException {
        File f = new File(reportPath);
        InputStream streamReport = JRLoader.getFileInputStream(f.getAbsolutePath());

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }
        reportParameter.put(JRPdfExporterParameter.METADATA_TITLE, getReportName(reportPath));

        if (params != null) {
            reportParameter.putAll(params);
        }
        reportParameter.put(JRParameter.IS_IGNORE_PAGINATION, true);

        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, con);
        htmlExporter(report, imageURI, Env.getContext().getHttpServletRequest(), out);
    }

    private static void htmlExporter(JasperPrint report, String imageURI, HttpServletRequest request, OutputStream out) throws JRException {
        HtmlExporter exporter = new HtmlExporter();
        request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, report);

        exporter.setExporterInput(new SimpleExporterInput(report));
        SimpleHtmlExporterOutput output = new SimpleHtmlExporterOutput(out);
        output.setImageHandler(new WebHtmlResourceHandler(imageURI + "{0}"));
        exporter.setExporterOutput(output);

        SimpleHtmlReportConfiguration reportExportConfiguration = new SimpleHtmlReportConfiguration();
        reportExportConfiguration.setSizeUnit(HtmlSizeUnitEnum.PIXEL);
        reportExportConfiguration.setOffsetX(0);
        reportExportConfiguration.setOffsetY(0);
        reportExportConfiguration.setIgnorePageMargins(Boolean.TRUE);
        reportExportConfiguration.setZoomRatio(1f);

        exporter.setConfiguration(reportExportConfiguration);
        exporter.exportReport();
    }

    public static void jasperToImage(String reportPath, String subReportPath, Map params, Connection con, OutputStream out) throws JRException, IOException {
        File f = new File(reportPath);
        InputStream streamReport = JRLoader.getFileInputStream(f.getAbsolutePath());

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }
        reportParameter.put(JRPdfExporterParameter.METADATA_TITLE, getReportName(reportPath));

        if (params != null) {
            reportParameter.putAll(params);
        }
        reportParameter.put(JRParameter.IS_IGNORE_PAGINATION, true);

        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, con);
        imageExporter(report, 1, out);
    }

    public static void jasperToImage(String reportPath, String subReportPath, Map params, Collection<Map<String, ?>> data, OutputStream out) throws JRException, IOException {
        File f = new File(reportPath);
        InputStream streamReport = JRLoader.getFileInputStream(f.getAbsolutePath());

        Map reportParameter = new HashMap();
        if (subReportPath != null) {
            reportParameter.put("SUBREPORT_DIR", subReportPath);
        }
        reportParameter.put(JRPdfExporterParameter.METADATA_TITLE, getReportName(reportPath));

        if (params != null) {
            reportParameter.putAll(params);
        }
        reportParameter.put(JRParameter.IS_IGNORE_PAGINATION, true);
        JRMapCollectionDataSource ds = new JRMapCollectionDataSource(data);
        JasperPrint report = JasperFillManager.fillReport(streamReport, reportParameter, ds);
        imageExporter(report, 1, out);
    }

    private static void imageExporter(JasperPrint report, int page, OutputStream out) throws JRException, IOException {
        imageExporter(report, page, page, out);
    }

    private static void imageExporter(JasperPrint report, int fromPage, int untilPage, OutputStream out) throws JRException, IOException {
        final String extension = "png";
        final float zoom = 1f;
        int pages = report.getPages().size();
        for (int i = fromPage; i <= untilPage; i++) {
            BufferedImage image = (BufferedImage) JasperPrintManager.printPageToImage(report, 0, zoom);
            ImageIO.write(image, extension, out); //write image to file
        }
    }
}
