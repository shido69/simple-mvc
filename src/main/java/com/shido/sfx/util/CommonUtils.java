/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import jxl.*;
import jxl.write.DateFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import net.sf.json.JSONNull;
import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author Shido69
 */
public class CommonUtils {

    public static String padString(String source) {
        char paddingChar = ' ';
        int size = 16;
        int padLength = size - source.length() % size;

        for (int i = 0; i < padLength; i++) {
            source += paddingChar;
        }

        return source;
    }

    public static Object getOrDefault(Map m, Object key, Object value) {
        Object v = null;
        if ((v = m.getOrDefault(key, value)) == null) {
            v = value;
        } else if (v instanceof JSONNull) {
            v = value;
        }
        return v;
    }
    private static Logger logger = Logger.getLogger(CommonUtils.class);

    private static Object parseNumberWithDecimal(Class type, Number test) {
        Object result;
        if (type == Float.class || type == float.class) {
            result = test.floatValue();
        } else if (type == Double.class || type == double.class) {
            result = test.doubleValue();
        } else {
            result = (Number) test;
        }
        return result;
    }

    private static Object parseNumberWithoutDecimal(Class type, Number test) {
        Object result;
        if (type == Byte.class || type == byte.class) {
            result = test.byteValue();
        } else if (type == Short.class || type == short.class) {
            result = test.shortValue();
        } else if (type == Integer.class || type == int.class) {
            result = test.intValue();
        } else if (type == Long.class || type == long.class) {
            result = test.longValue();
        } else {
            result = parseNumberWithDecimal(type, test);
        }
        return result;
    }

    public static Object parseParameterValue(Class type, String value) {
        Object result = null;
        if (value == null) {
            return null;
        }
        if (type == String.class) {
            result = value.isEmpty() ? null : value;
        } else if (type == java.sql.Timestamp.class) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                result = sdf.parse(value);
            } catch (ParseException e) {
            }
        } else if (type == java.sql.Time.class) {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            try {
                result = sdf.parse(value);
            } catch (ParseException e) {
            }
        } else if (type == Date.class) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                result = sdf.parse(value);
            } catch (ParseException e) {
            }
        } else if (NumberUtils.isNumber(value)) {
            BigDecimal test = NumberUtils.createBigDecimal(value);
            if (test.scale() <= 0) {
                result = parseNumberWithoutDecimal(type, test);
            } else {
                result = parseNumberWithDecimal(type, test);
            }
        } else {
            try {
                result = type.getDeclaredConstructor(new Class[]{value.getClass()}).newInstance(new Object[]{value});
            } catch (Exception e) {
                logger.error(e);
//                result = value;
            }
        }
        return result;
    }

    public static String cleanFieldName(String fieldName) {
        return cleanFieldName(
                new String[]{"m", "c", "doc", "sys"},
                new String[]{"id"},
                fieldName);
    }

    public static String cleanFieldName(String[] prefix, String[] suffix, String fieldName) {
        String clean = fieldName.toLowerCase();
        for (String p : prefix) {
            if (clean.startsWith(p.toLowerCase() + "_")) {
                clean = clean.substring(clean.indexOf("_") + 1);
            }
            clean = clean.replace("_" + p + "_", "_");
        }
        for (String s : suffix) {
            if (clean.endsWith("_" + s.toLowerCase())) {
                clean = clean.substring(0, clean.length() - ("_" + s).length());
            }
        }
        String split[] = clean.split("_");
        clean = "";
        for (int i = 0; i < split.length; i++) {
            if (!split[i].isEmpty()) {
                logger.debug("cleaning..." + split[i]);
                clean += Character.toUpperCase(split[i].charAt(0)) + split[i].substring(1);
                if ((i + 1) < split.length) {
                    clean += " ";
                }
            }
        }
        return clean;
    }

    public static List stringToList(String text, String separator) {
        List<String> result = new ArrayList<String>();
        for (String s : text.split(separator)) {
            result.add(s);
        }
        return result;
    }

    public static String createTemplate(String title, String subContent, String sitePath, String content, String additionalHeader, String additionalFooter) {
        return createTemplate("blank", title, subContent, sitePath, content, additionalHeader, additionalFooter);
    }

    public static String createTemplate(String base, String title, String subContent, String sitePath, String content, String additionalHeader, String additionalFooter) {
        HttpServletRequest request = Env.getContext().getHttpServletRequest();
        if (additionalHeader != null) {
            if (!additionalHeader.isEmpty()) {
                Env.setAdditionalHeader(additionalHeader);
            }
        }
        if (additionalFooter != null) {
            if (!additionalFooter.isEmpty()) {
                Env.setAdditionalFooter(additionalFooter);
            }
        }
        request.setAttribute("pageTitle", title);
        String scontent = subContent == null ? null : subContent.isEmpty() ? null : subContent;
        request.setAttribute("pageSubTitle", scontent);
        request.setAttribute("subContent", scontent);
        if (sitePath != null) {
            if (!sitePath.isEmpty()) {
                request.setAttribute("sitePath", CommonUtils.stringToList(sitePath, "/"));
            }
        }
        request.setAttribute("pageContent", content);
        return base;
    }

    public static boolean isStringExistsInArray(String[] src, String check) {
        boolean result = false;
        if (src != null) {
            for (String s : src) {
                if (s.trim().equalsIgnoreCase(check.trim())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public static String toInString(List arr) {
        Object sarr[] = (Object[]) arr.toArray(new Object[arr.size()]);
        return toInString(sarr);
    }

    public static String toInString(Object[] arr) {
        return toInString(arr, null);
    }

    public static boolean isInQueryString(String query, String key) {
        String[] nameValuePairs = query.split("&");
        for (String nameValuePair : nameValuePairs) {
            if (nameValuePair.startsWith(key + "=")) {
                return true;
            }
        }
        return false;
    }

    public static String toInString(Object[] arr, String encloseChar) {
        String ec = encloseChar == null ? "'" : encloseChar;
        String result = "";
        boolean number;
        for (int i = 0; i < arr.length; i++) {
            number = arr[i] instanceof Number;
            String in = number ? ((Number) arr[i]).toString() : (String) arr[i];
            result += number ? in : ec + in + ec;
            if (i + 1 < arr.length) {
                result += ",";
            }
        }
        return result;
    }

    public static String toInString(String string, String sep) {
        String[] arr = string.split(sep);
        return toInString(arr);
    }

    public static int countInterval(int field, Calendar f, Calendar u, int... exclude) {
        return countInterval(new HashMap<>(), field, f, u, exclude);
    }

    public static int countInterval(int field, Date f, Date u, int... exclude) {
        return countInterval(new HashMap<>(), field, f, u, exclude);
    }

    public static int countInterval(Map<Date, Boolean> check, int field, Date f, Date u, int... exclude) {
        Calendar from = Calendar.getInstance();
        from.setTime(f);
        Calendar until = Calendar.getInstance();
        until.setTime(u);
        return countInterval(check, field, from, until, exclude);
    }

    public static int countInterval(Map<Date, Boolean> check, int field, Calendar f, Calendar u, int... exclude) {
        int count = 0;
        Calendar counter = (Calendar) f.clone();
        boolean end;
        while (end = counter.compareTo(u) < 1) {
            boolean ok = check.getOrDefault(counter.getTime(), Boolean.TRUE);
            logger.info("same date check: " + ok);
            if (ok) {
                int dow = counter.get(Calendar.DAY_OF_WEEK);
                boolean excluded = false;
                for (int x : exclude) {
                    if (dow == x) {
                        excluded = true;
                        break;
                    }
                }
                if (!excluded) {
                    count++;
                }
                check.put(counter.getTime(), Boolean.FALSE);
            } else {
                logger.info("skip same date");
            }
            counter.add(field, 1);
        }
        logger.info(String.format("Count Interval %tF to %tF = %d", f, u, count));
        return count;
    }

    public static String join(String delim, String[] strings) {
        String result = "";
        for (String s : strings) {
            result += delim + s;
        }
        return result.isEmpty() ? "" : result.substring(1);
    }

    public static boolean stringArrayContains(String cmdinfo, String[] arr) {
        boolean result = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equalsIgnoreCase("CMD=INFO")) {
                result = true;
                break;
            }
        }
        return result;
    }

    public static void listMapToXLS(List<Map> theList, OutputStream outputStream) throws Exception {
        if (theList.size() < 1) {
            return;
        }
        Set headers = ((Map) theList.get(0)).keySet();
        Map theHeaders = new LinkedHashMap();
        for (Iterator<String> it = headers.iterator(); it.hasNext();) {
            String key = it.next();
            theHeaders.put(key, CommonUtils.cleanFieldName(key));
        }
        listMapToXLS(theHeaders, theList, outputStream);
    }

    public static void listMapToXLS(Map keyLabel, List<Map> theList, OutputStream outputStream) throws Exception {
        if (theList == null) {
            return;
        }

        WorkbookSettings ws = new WorkbookSettings();
        ws.setLocale(new Locale("en", "EN"));
        OutputStream os = outputStream;
        WritableSheet sheet1 = null;
        WritableWorkbook workBook = null;
        DateFormat df = new DateFormat("dd/MM/yyyy hh:mm:ss");
        df.getDateFormat().setTimeZone(TimeZone.getDefault());
        WritableCellFormat dateFormat = new WritableCellFormat(df);
        try {

            workBook = Workbook.createWorkbook(os, ws);
            workBook.createSheet("Sheet1", 0);
            sheet1 = workBook.getSheet(0);

            int row = 0;
            int i = 0;
            for (Iterator<String> keyIt = keyLabel.keySet().iterator(); keyIt.hasNext();) {
                String key = keyIt.next();
                sheet1.addCell(new Label(i++, row, (String) keyLabel.get(key)));
            }
            row++;
            for (Map y : theList) {
                i = 0;
                for (Iterator<String> keyIt = keyLabel.keySet().iterator(); keyIt.hasNext();) {
                    try {
                        String key = keyIt.next();
                        if (y.get(key) instanceof java.lang.Number) {
                            sheet1.addCell(new jxl.write.Number(i, row, ((Number) y.get(key)).doubleValue()));
                        } else if (y.get(key) instanceof Date) {
                            sheet1.addCell(new DateTime(i, row, (Date) y.get(key), dateFormat));
                        } else {
                            sheet1.addCell(new Label(i, row, y.get(key).toString()));
                        }
                    } catch (NullPointerException e) {
                        logger.debug("null value... skip");
                    }
                    i++;
                }
                row++;
            }

        } catch (Exception e) {
            logger.debug("JXL Initialization Error!");
            throw e;

        } finally {
            try {
                workBook.write();
                workBook.close();
            } catch (Exception ex) {
                throw ex;
            }
        }
    }

    public static void generateQRtoFile(String text, int width, int height, File out) throws FileNotFoundException, WriterException, IOException {
        FileOutputStream fout = new FileOutputStream(out);
        generateQR(text, width, height, fout);
    }

    public static void generateQR(String text, int width, int height, OutputStream out) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", out);
    }

    private static void testqr() {
        String path = "C:/Users/lenovo/Pictures/foto/testqr.png";
        File out = new File(path);
        try {
            generateQRtoFile(path, 320, 320, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean copyFile(File ori, File dest) {
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            try {
                in = new FileInputStream(ori);
                out = new FileOutputStream(dest);
                int MAX_BUFF = 4096;
                byte[] buff = new byte[MAX_BUFF];
                int pos = 0;
                while ((pos = in.read(buff)) > 0) {
                    out.write(buff, 0, pos > MAX_BUFF ? MAX_BUFF : pos);
                }
                return true;
            } catch (FileNotFoundException e) {
                return false;
            } finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public static Object getBytes(File image) throws IOException, FileNotFoundException {
        FileInputStream fis = new FileInputStream(image);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(fis.available());
        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) > 0) {
            baos.write(buffer, 0, len);
        }
        fis.close();
        byte[] result = baos.toByteArray();
        baos.flush();
        baos.close();
        return result;
    }

    public static Map listToMap(List<Map> list, String key, String value) {
        Map result = null;
        if (list != null) {
            result = new LinkedHashMap();
            for (Map m : list) {
                if (value.equals("*")) {
                    List inner;
                    if (result.get(m.get(key)) == null) {
                        inner = new ArrayList();
                    } else {
                        inner = (List) result.get(m.get(key));
                    }
                    inner.add(m);
                    result.put(m.get(key), inner);
                } else {
                    result.put(m.get(key), m.get(value));
                }
            }
        }
        return result;
    }

    static boolean isPathMatch(String src, String check) {
        boolean result = true;
        if (src == null || check == null) {
            return false;
        }
        String[] asrc = src.split("/"), acheck = check.split("/");
        result = result && asrc.length == acheck.length;
        if (result) {
            for (int i = 0; i < acheck.length; i++) {
                String c = acheck[i];
                String s = asrc[i];
                if (c.startsWith(":")) {
                    continue;
                }
                result = result && c.equals(s);
            }
        }
        return result;
    }

    public static boolean isStringContaintsWholeWord(String src, String substr) {
        String pattern = "\\b" + substr + "\\b";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(src);
        return m.find();
    }

}
