/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import com.shido.sfx.annotations.Controller;
import com.shido.sfx.annotations.Route;
import com.shido.sfx.annotations.UrlParameter;
import common.Logger;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Amaran Sidhiq
 */
public class ClassScanner {

    public static Class[] getSFXControllerClass(String scanLoc)
            throws ClassNotFoundException, IOException {
        Class[] classes = ClassScanner.getClasses(scanLoc);
        List<Class> temp = new ArrayList();

        for (Class c : classes) {
            if (c.isAnnotationPresent(Controller.class)) {
                temp.add(c);
            }
        }
        classes = temp.isEmpty() ? null : temp.toArray(new Class[temp.size()]);
        temp.clear();
        return classes;
    }
    private static Logger logger = Logger.getLogger(ClassScanner.class);

    public static Method getSFXMethod(Class controller, String routePath, String method) {
        Method result = null;
        boolean valid;
        for (Method m : controller.getDeclaredMethods()) {
            if (m.isAnnotationPresent(Route.class)) {
                Route route = m.getAnnotation(Route.class);
                valid = method.equalsIgnoreCase(route.method().toString());
                valid = valid || route.method() == com.shido.sfx.annotations.Method.ALL;
                logger.debug(routePath + " - " + method + ":" + valid);
                valid = valid && CommonUtils.isPathMatch(routePath, route.value());
                if (valid) {
                    result = m;
                    break;
                }
            }
            result = null;
        }
        return result;
    }

    /**
     * Scans all classes accessible from the context class loader which belong
     * to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and
     * subdirs.
     *
     * @param directory The base directory
     * @param packageName The package name for classes found inside the base
     * directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles((dir, name) -> {
            return new File(dir, name).isDirectory() || (name.endsWith(".class") && !name.contains("$"));
        });
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

    public static Object[] getParameterValues(Method route, String mvcCallPath) {
        Object[] result = null;
        if (route != null) {
            Route routeAnnot = route.getAnnotation(Route.class);
            String routeAnnotPath = routeAnnot.value();
            if (routeAnnotPath.contains("/:")) {
                String[] params = mvcCallPath.substring(routeAnnotPath.indexOf("/")).split("/");
                String[] urlParams = routeAnnotPath.substring(routeAnnotPath.indexOf("/")).split("/");
                Parameter[] parameters = route.getParameters();
                if (params.length == urlParams.length) {
                    result = new Object[parameters.length];
                    for (int pi = 0; pi < parameters.length; pi++) {
                        Parameter p = parameters[pi];
                        for (int i = 0; i < urlParams.length; i++) {
                            String urlParam = urlParams[i];
                            if (!urlParam.startsWith(":")) {
                                continue;
                            }
                            urlParam = urlParam.substring(1);
                            if ((p.isAnnotationPresent(UrlParameter.class) && p.getAnnotation(UrlParameter.class).value().equals(urlParam))
                                    || p.getName().equals(urlParam)) {
                                result[pi] = CommonUtils.parseParameterValue(p.getType(), params[i]);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
}
