/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Amaran Sidhiq
 */
public class AESDecryption {

    private Cipher cipher;

    public AESDecryption(AESKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        this(key.getKey(), key.getIvspec());
    }

    public AESDecryption(Key keyspec, AlgorithmParameterSpec ivspec) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
    }

    public String decrypt(String code) throws IllegalBlockSizeException, BadPaddingException, IOException {
        Base64.Decoder decoder=Base64.getMimeDecoder();
        byte[] decodedMessage = decoder.decode(code);

        byte[] outText = cipher.doFinal(decodedMessage);
        return new String(outText).trim();
    }
}
