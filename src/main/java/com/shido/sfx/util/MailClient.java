/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class MailClient {
    private static Logger logger= Logger.getLogger(MailClient.class);
    private String from;
    private ArrayList<InternetAddress> bcc, cc, to;
    private String subject;
    String mailhost = null;
    boolean debug = false;
    String status;

    public MailClient() {
        try {
            setMailHost(Env.getConfig().getProperty("sfx.mail.host", "smtp.gmail.com"));
            setMailProperties("mail.smtp.port", Env.getConfig().getProperty("sfx.mail.port", "587"));
            setMailProperties("mail.smtp.auth", Env.getConfig().getProperty("sfx.mail.auth", "true"));
            setMailProperties("mail.smtp.starttls.enable", Env.getConfig().getProperty("sfx.mail.tls", "true")); //TLS
        } catch (NullPointerException e) {
            setMailHost("smtp.gmail.com");
            setMailProperties("mail.smtp.port", "587");
            setMailProperties("mail.smtp.auth", "true");
            setMailProperties("mail.smtp.starttls.enable", "true"); //TLS
        }
    }

    public final void setMailHost(String mailhost) {
        this.mailhost = mailhost;
        setMailProperties("mail.smtp.host", mailhost);
    }

    public String getMailHost() {
        return this.mailhost;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return this.from;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setMessage(String message) {
        this.messageText = new StringBuilder(message);
    }

    public void setMessage(File template) throws IOException {
        this.messageText = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(template));
        String temp = null;
        while ((temp = reader.readLine()) != null) {
            this.messageText.append(temp);
            this.messageText.append("\n");
        }
    }

    private Map<String, Object> multiPartMessage;

    private boolean isMultipart() {
        return multiPartMessage != null;
    }

    public void setMultipart(boolean val) {
        if (val) {
            if (this.multiPartMessage == null) {
                this.multiPartMessage = new LinkedHashMap();
            }
            if (this.messageText != null) {
                this.multiPartMessage.put("body", getMessage());
            }
        } else {
            if (this.multiPartMessage != null) {
                setMessage((String) this.multiPartMessage.get("body"));
                this.multiPartMessage.clear();
                this.multiPartMessage = null;
            }

        }
    }

    public void setMultiPart(String key, Object multipart) {
        setMultipart(true);
        multiPartMessage.put(key, multipart);
    }
    private StringBuilder messageText;

    public String getMessage() {
        return this.messageText.toString();
    }

    public void setCC(String cc) {
        this.cc = new ArrayList();
        if (cc.length() < 1) {
            return;
        }
        String[] ccs = cc.split(",");
        try {
            for (int i = 0; i < ccs.length; i++) {
                this.cc.add(new InternetAddress(ccs[i]));
            }
        } catch (Exception e) {
           logger.error("Error setting carbon copy!", e);
        }
    }

    public void setBCC(String bcc) {
        this.bcc = new ArrayList();
        if (bcc.length() < 1) {
            return;
        }
        String[] bccs = bcc.split(",");
        try {
            for (int i = 0; i < bccs.length; i++) {
                this.bcc.add(new InternetAddress(bccs[i]));
            }
        } catch (Exception e) {
            logger.error("Error setting carbon copy!", e);
        }
    }

    public void setTo(String to) {
        this.to = new ArrayList();
        processAddress(this.to, to);
    }

    private void processAddress(ArrayList to, String addrs) {
        String[] tos = addrs.split(",");

        try {
            for (int i = 0; i < tos.length; i++) {
                to.add(new InternetAddress(tos[i]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void appendTo(String to) {
        if (this.to == null) {
            this.to = new ArrayList();
        }
        processAddress(this.to, to);

    }

    public InternetAddress[] getTo() {
        InternetAddress[] tos = new InternetAddress[this.to.size()];
        for (int i = 0; i < this.to.size(); i++) {
            tos[i] = to.get(i);
        }
        return tos;
    }

    public InternetAddress[] getCC() {
        if (this.cc == null) {
            return null;
        }
        InternetAddress[] ccs = new InternetAddress[this.cc.size()];
        for (int i = 0; i < this.cc.size(); i++) {
            ccs[i] = cc.get(i);
        }
        return ccs;
    }

    public InternetAddress[] getBCC() {
        if (this.bcc == null) {
            return null;
        }
        InternetAddress[] ccs = new InternetAddress[this.bcc.size()];
        for (int i = 0; i < this.bcc.size(); i++) {
            ccs[i] = bcc.get(i);
        }
        return ccs;
    }
    private String contentType = "text/html";

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    Properties props = new Properties();

    public Object getMailProperties(String key) {
        return props.get(key);
    }

    public void setMailProperties(String key, Object value) {
        props.put(key, value);
    }
    private String username, password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean sendMessage() throws Exception {
        try {
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
            session.setDebug(debug);

            // create a message
            MimeMessage mail = new MimeMessage(session);

            // set the from and to address
            InternetAddress addressFrom = new InternetAddress(from);
            mail.setFrom(addressFrom);
            mail.setRecipients(Message.RecipientType.TO, getTo());
            if (getCC() != null) {
                mail.setRecipients(Message.RecipientType.CC, getCC());
            }
            if (getBCC() != null) {
                mail.setRecipients(Message.RecipientType.BCC, getBCC());
            }

            mail.setSubject(subject);
            mail.setContent(messageText.toString(), contentType);
            if (multiPartMessage != null) {
                if (multiPartMessage.size() > 1) {
                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart messageBodyPart;
                    for (Iterator<String> it = multiPartMessage.keySet().iterator(); it.hasNext();) {
                        String key = it.next();
                        Object message = multiPartMessage.get(key);
                        messageBodyPart = new MimeBodyPart();
                        if (key.equals("body") && message.getClass().equals(String.class)) {
                            messageBodyPart.setContent(multiPartMessage.get(key), contentType);
                        } else if (message.getClass().equals(java.io.File.class)) {
                            File fileAttachment = (File) message;
                            DataSource source = new FileDataSource(fileAttachment);
                            messageBodyPart.setHeader("Content-ID", "<" + key + ">");
                            messageBodyPart.setDataHandler(new DataHandler(source));
                            messageBodyPart.setFileName(fileAttachment.getName());
                            String fileExtension = fileAttachment.getName().substring(fileAttachment.getName().lastIndexOf(".")).toLowerCase();
                            if (fileExtension.endsWith("jpg") || fileExtension.endsWith("png")) {
                                messageBodyPart.setDisposition(MimeBodyPart.INLINE);
                            } else {
                                messageBodyPart.setDisposition(MimeBodyPart.ATTACHMENT + "; filename=" + fileAttachment.getName());
                            }
                        }
                        multipart.addBodyPart(messageBodyPart);
                    }
                    mail.setContent(multipart);
                }
            }
            // Setting the Subject and Content Type
            mail.saveChanges();
            Transport.send(mail);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending email!", ex);
            throw new Exception("Error sending email!", ex);
        } finally {
        }
    }

    public void setStatus(String statusString) {
        this.status = statusString;
    }

    public String getStatus() {
        return this.status;
    }

    public static void main(String[] args) {
        MailClient mc = new MailClient();
        mc.setUsername("webmaster.niagasaya@gmail.com");
        mc.setPassword("swxwenrdnvsablcw");

        mc.setFrom("CS Smartlife <cs@niagasaya.com>");
        mc.setTo("webmaster.niagasaya@gmail.com");

        try {
            mc.setMessage(new File("mailtemp.html"));
        } catch (IOException ex) {
            logger.warn("No Template", ex);
        }
        mc.setSubject("Selamat datang di smartlife! Selangkah Lagi...");
        
        File attachment = new File("D:\\Project\\niagasaya_img\\2.jpg");
        logger.debug(attachment.exists());
        mc.setMultiPart("image", attachment);
        try {
            mc.sendMessage();
        } catch (Exception ex) {
            logger.error("cannot send", ex);
        }
    }
}
