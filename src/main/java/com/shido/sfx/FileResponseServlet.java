/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
@WebServlet(
        name = "FileResponseHandler",
        urlPatterns = {"/files/*"}
)
public class FileResponseServlet extends MVCRequestHandler {
    private static Logger logger=Logger.getLogger(FileResponseServlet.class);
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String location = Env.getConfig().getProperty(Env.FILE_UPLOAD_LOCATION);
        String file = request.getPathInfo();
        File theFile = new File(location + File.separator + file);
        logger.debug("File exists:" + theFile.exists());
        if (!theFile.exists()) {
            String noImg = request.getParameter("noimg=true");
            noImg = noImg == null ? "true" : "false";
            if (noImg.equalsIgnoreCase("true")) {
                theFile = new File(request.getServletContext().getRealPath("/assets/img/no-image.png"));
            } else {
                response.sendError(404);
                return;
            }
        }
        logger.debug("Used File:"+theFile.toString());
        URLConnection url = theFile.toURI().toURL().openConnection();
        String contentType = url.getContentType();
        response.setContentType(contentType);

        FileInputStream in = new FileInputStream(theFile);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStream os = response.getOutputStream();
        try {
            int buff_size = 4096;
            int len;
            byte[] buff = new byte[buff_size];
            while ((len = in.read(buff)) > 0) {
                baos.write(buff, 0, len > buff_size ? buff_size : len);
            }
            os.write(baos.toByteArray());
            baos.flush();
        } finally {
            baos.close();
            os.close();
            in.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
