/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.Env;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public class ImageColumn extends BasicRenderer {

    private String imageURL = null;

    public ImageColumn() {

    }

    public ImageColumn(String url) {
        imageURL = url;
    }

    @Override
    public String render(String fieldName, String labelText, Map rowData, Object value) {
        StringBuilder field = new StringBuilder();
        if (imageURL == null) {
            if (value != null) {
                imageURL = Env.getBaseURI() + "/" + value.toString();
            }
        }
        if (imageURL != null) {
            field.append("<img class='img-fluid' src='").append(imageURL).append("' />");
        }
        return field.toString();
    }

}
