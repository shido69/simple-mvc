/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public interface ColumnRenderer{

    public String render(Integer rowIndex, Map rowData, Object value);

}
