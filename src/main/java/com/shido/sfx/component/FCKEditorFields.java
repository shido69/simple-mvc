/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.Env;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class FCKEditorFields extends BasicRenderer {

    public FCKEditorFields(String label) {
        setLabel(label);
    }
    private static Logger logger = Logger.getLogger(FCKEditorFields.class);

    @Override
    public String render(String fieldName, String fieldLabel, Map rowData, Object value) {
        StringBuilder field = new StringBuilder();
        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }
        logger.info(fieldName + "(FCK):" + value);
        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");
        field.append("<textarea name='").append(fieldName).append("' id=\"").append(fieldName).append("\" class='form-control'>").append(value == null ? "" : value).append("</textarea>\n");
        field.append("                    <script>\n");
        field.append("                        CKEDITOR.replace('").append(fieldName).append("', {\n");
        field.append("                            // Define the toolbar groups as it is a more accessible solution.\n");
        field.append("                            toolbarGroups: [\n");
        field.append("                                {\"name\": \"basicstyles\", \"groups\": [\"basicstyles\"]},\n");
        field.append("                                {\"name\": \"links\", \"groups\": [\"links\"]},\n");
        field.append("                                {\"name\": \"paragraph\", \"groups\": [\"list\", \"blocks\"]},\n");
        field.append("                                {\"name\": \"document\", \"groups\": [\"mode\"]},\n");
        field.append("                                {\"name\": \"insert\", \"groups\": [\"insert\"]},\n");
        field.append("                                {\"name\": \"styles\", \"groups\": [\"styles\"]},\n");
        field.append("                                {\"name\": \"about\", \"groups\": [\"about\"]}\n");
        field.append("                            ],\n");
        field.append("                            // Remove the redundant buttons from toolbar groups defined above.\n");
        field.append("                            removeButtons: 'Save,Flash,Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'\n");
        field.append("                        });\n");
        field.append("                    </script>");
        field.append("</div>\n");
        return field.toString();
    }

}
