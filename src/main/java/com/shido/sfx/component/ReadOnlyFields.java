/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public class ReadOnlyFields extends BasicRenderer {

    public ReadOnlyFields() {

    }

    public ReadOnlyFields(String label) {
        setLabel(label);
    }

    @Override
    public String render(String fieldName, String labelText, Map rowData, Object value){
        StringBuilder field = new StringBuilder();
        String label = labelText;
        if (label == null) {
            label = fieldName;
        }
        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");
        field.append("<div class='bg-white form-control' style='min-height:30px;padding:2px 2px 2px 10px;border:solid 1px #ccc; ").append(getElementStyle()).append("'>\n");
        if (value != null) {
            field.append(value);
        }
        field.append("</div>\n");
        field.append("</div>\n");
        return field.toString();
    }

}
