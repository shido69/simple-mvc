/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.Env;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public class DateField extends BasicRenderer {

    public DateField(String fieldName, String label) {
        setFieldName(fieldName);
        setLabel(label);
    }


    public String renderx(String fieldName, String fieldLabel, Map rowData, Object value) {
        HTMLComponent formGroup=new HTMLComponent("div");
        formGroup.addStyleClass("form-group");
        
        HTMLComponent label=new HTMLComponent("label",fieldLabel);
        label.setAttribute("for", fieldName);
        formGroup.appendChild(label);
        
        HTMLComponent inputGroupDate = new HTMLComponent("div");
        inputGroupDate.addStyleClass("input-group date");
        formGroup.appendChild(inputGroupDate);
        
        HTMLComponent inputGroupPrepend = new HTMLComponent("div");
        inputGroupPrepend.addStyleClass("input-group-prepend");
        inputGroupDate.appendChild(inputGroupPrepend);

        HTMLComponent inputGroupText = new HTMLComponent("span");
        inputGroupText.addStyleClass("input-group-text");
        inputGroupPrepend.appendChild(inputGroupText);
        
        HTMLComponent calendarIcon =  new HTMLComponent("i");
        calendarIcon.addStyleClass("fa fa-calendar");
        inputGroupText.appendChild(calendarIcon);
        
        HTMLComponent input=new HTMLComponent("input");
        input.setAttribute("type", "date");
        input.setAttribute("style", getStyleClasses());
        input.addStyleClass("form-control pull-right datepicker");
        input.setAttribute("value", String.format("%tF", new Date(((java.sql.Date) value).getTime())));
        inputGroupDate.appendChild(input);
        
        return formGroup.toString();
    }
    public String render(String fieldName, String fieldLabel, Map rowData, Object value) {
        StringBuilder field = new StringBuilder();
        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }

        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");

        field.append("<div class='input-group date'>\n");
        field.append("<div class='input-group-prepend'>\n");
        field.append("<span class='input-group-text'><i class='fa fa-calendar'></i></span>\n");
        field.append("</div>\n");
        field.append("<input type='date' style='").append(getElementStyle()).append("' class='form-control pull-right datepicker' id='").append(fieldName).append("' name='").append(fieldName).append("' value='\n");
        if (value != null) {
            field.append(String.format("%tF", new Date(((java.sql.Date) value).getTime())));
        }
        field.append("'>\n");
        field.append("</div>\n");
        field.append("</div>\n");
        return field.toString();
    }

}
