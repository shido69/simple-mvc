/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.CommonUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class FormBuilder {

    Map<String, Renderer> customRenderer;
    Map<String, Boolean> readOnlyFields;
    Map<String, Boolean> hiddenFields;
    private Map fields;
    private Map data;
    private String action;
    private String classes;
    private String method;

    public void setClasses(String classes) {
        this.classes = classes;
    }
    private String id;

    public FormBuilder(String id, Map data, String action, String method) throws NullPointerException {
        this(id, null, data, action, method);
        this.fields = new LinkedHashMap();
        if (data != null) {
            Set<String> keySet = (Set<String>) data.keySet();
            for (String key : keySet) {
                this.fields.put(key, CommonUtils.cleanFieldName(key));
            }
        }
    }

    public FormBuilder(String id, Map fields, Map data, String action, String method) throws NullPointerException {
        this.customRenderer = new HashMap();
        this.readOnlyFields = new HashMap();
        this.hiddenFields = new HashMap();
        this.id = id;
        this.fields = fields;
        this.data = data;
        this.action = action;
        this.method = method;
    }
    private boolean multiPart = false;

    public void setMultipart(boolean b) {
        this.multiPart = b;
    }

    public void setFieldRenderer(String fieldName, Renderer renderer) {
        this.customRenderer.put(fieldName, renderer);
    }

    public void setHidden(String fieldName, boolean hidden) {
        this.hiddenFields.put(fieldName, hidden);
    }

    public void setReadOnly(String fieldName, boolean readOnly) {
        this.readOnlyFields.put(fieldName, readOnly);
    }

    private String cleanFieldName(String fieldName) {
        return CommonUtils.cleanFieldName(fieldName);
    }

    private boolean isReadOnly(String fieldName) {
        Boolean result = this.readOnlyFields.get(fieldName);
        return result != null ? result : false;
    }

    private boolean isVisible(String fieldName) {
        Boolean result = this.hiddenFields.get(fieldName);
        return result != null ? !result : true;
    }
    private String defaultFieldClasses = "";
    private static final Logger logger = Logger.getLogger(FormBuilder.class);

    @Override
    public String toString() {
        return render();
    }
    StringBuilder form;

    public String render() {
        return render(false);
    }
    private BasicRenderer defaultRenderer = new GenericTextField();

    public void setDefaultRenderer(BasicRenderer defaultRenderer) {
        this.defaultRenderer = defaultRenderer;
    }
    private static final String FORM_OPEN_TAG = "<div class='card card-body'><form id='%1$s' name='%1$s' action='%2$s' class='%3$s' method='%4$s' %5$s>";
    private static final String FORM_CLOSE_TAG = "</form></div>";
    private static final String FORM_MULTIPART_ENC = "enctype='multipart/form-data'";
    private boolean renderFormElementOnly = false;

    public boolean isRenderFormElementOnly() {
        return renderFormElementOnly;
    }

    public void setRenderFormElementOnly(boolean renderFormElementOnly) {
        this.renderFormElementOnly = renderFormElementOnly;
    }

    protected String render(boolean redraw) {
        if (form == null || redraw) {
            logger.debug("Rendering Form" + (redraw ? "(Forced)" : ""));
            form = new StringBuilder();
            int i = 0;
            if (data == null) {
                data = new HashMap();
            }
            if (!renderFormElementOnly) {
                form.append(String.format(FORM_OPEN_TAG, this.id + i, this.action, this.classes, this.method, this.multiPart ? FORM_MULTIPART_ENC : ""));
            }

            BasicRenderer hidden = new HiddenField();
            for (Map.Entry<String, String> field : (Set<Map.Entry<String, String>>) fields.entrySet()) {
                Renderer renderer;
                if (!isVisible(field.getKey())) {
                    renderer = hidden;
                } else {
                    renderer = this.customRenderer.get(field.getKey());
                    if (renderer == null) {
                        renderer = defaultRenderer;
                    }
                }
                if (this.readOnlyFields.getOrDefault(field.getKey(), false)) {
                    renderer = new ReadOnlyFields();
                }
                logger.debug("Rendering " + field.getKey() + ":" + renderer.getClass());
                if (renderer instanceof BasicRenderer) {
                    ((BasicRenderer) renderer).setStyleClass(defaultFieldClasses);
                    ((BasicRenderer) renderer).setReadOnly(isReadOnly(field.getKey()));
                    ((BasicRenderer) renderer).setFieldName(field.getKey());
                    ((BasicRenderer) renderer).setLabel(field.getValue());
                    ((BasicRenderer) renderer).setRowData(data);
                    ((BasicRenderer) renderer).setValue(data.get(field.getKey()));
                    form.append(((BasicRenderer) renderer).render());
                } else {
                    form.append(renderer.render(field.getKey(), field.getValue(), data, data.get(field.getKey())));
                }
            }
            if (submitButton != null) {
                form.append("<button class='").append(submitButtonStyleClass).append("' name='").append(this.id).append("Submit'>").append(submitButtonIconClass != null ? "<i class='" + submitButtonIconClass + "'></i> " : "").append(submitButton).append("</button>");
            }
            if (!renderFormElementOnly) {
                form.append(FORM_CLOSE_TAG);
            }
        }
        return form.toString();
    }

    public void setDefaultFieldClass(String classes) {
        this.defaultFieldClasses = classes;
    }
    private String submitButton = null;
    private String submitButtonStyleClass;
    private String submitButtonIconClass;

    public void setSubmitButton(String text) {
        setSubmitButton(text, "", "");
    }

    public void setSubmitButton(String text, String styleClass) {
        setSubmitButton(text, styleClass, "");
    }

    public void setSubmitButton(String text, String styleClass, String iconClass) {
        this.submitButton = text;
        this.submitButtonStyleClass = styleClass;
        this.submitButtonIconClass = iconClass;
    }
}
