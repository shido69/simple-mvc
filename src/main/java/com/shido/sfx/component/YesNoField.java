/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.Env;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class YesNoField extends BasicRenderer {

    private String label;

    public YesNoField() {
    }

    public YesNoField(String fieldName, String label) {
        this(fieldName, label, null);
    }

    public YesNoField(String fieldName, String label, Map rowData) {
        setFieldName(fieldName);
        setLabel(label);
        setRowData(rowData);
    }
    private static final Logger logger = Logger.getLogger(YesNoField.class);

    @Override
    public String render(String fieldName, String labelText, Map rowData, Object value) {
        StringBuilder field = new StringBuilder();
        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }

        String val = "Y";
        if (value != null) {
            val = (String) value;
        }
        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");

        field.append("<div class='radio'>\n");
        field.append("<label>\n");
        field.append("<input type=\"radio\" value='Y' name=\"").append(fieldName).append("\"").append(val.equalsIgnoreCase("Y") ? "checked" : "").append("/>");
        field.append("Yes\n");
        field.append("</label>\n");
        field.append("<label>\n");
        field.append("&nbsp;<input type=\"radio\" value='N' name=\"").append(fieldName).append("\"").append(val.equalsIgnoreCase("N") ? "checked" : "").append("/>");
        field.append("No\n");
        field.append("</label>\n");
        field.append("</div>\n");
        field.append("</div>\n");
        return field.toString();
    }


}
