/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.Env;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public class TimePickerField extends BasicRenderer {

    public TimePickerField(String fieldName, String label) {
        setFieldName(fieldName);
        setLabel(label);
    }

    @Override
    public String render(String fieldName, String labelText, Map rowData, Object value) {
        StringBuilder field = new StringBuilder();
        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }

        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");

        field.append("<div class='input-group'>\n");
        field.append("<input type='time' style='").append(getElementStyle()).append("' class='field-control timepicker' id='").append(fieldName).append("' name='").append(fieldName).append("' value='\n");
        if (value != null) {
            field.append(String.format("%tT", new Date(((java.sql.Date) value).getTime())));
        }
        field.append("'>\n");
        field.append("<div class=\"input-group-addon\">\n");
        field.append("<i class=\"fa fa-clock-o\"></i>\n");
        field.append("</div>\n");
        field.append("</div>\n");
        field.append("</div>\n");
        return field.toString();
    }

}
