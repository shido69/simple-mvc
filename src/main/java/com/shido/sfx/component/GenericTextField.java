/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class GenericTextField extends BasicRenderer {

    public GenericTextField() {
    }

    public GenericTextField(String fieldName, String label) {
        setFieldName(fieldName);
        setLabel(label);
    }

    private static Logger logger = Logger.getLogger(GenericTextField.class);

    @Override
    public String render(String fieldName, String labelText, Map rowData, Object value) {
        StringBuilder field = new StringBuilder();
        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }

        logger.debug(fieldName + "(" + (value == null ? "Generic" : value.getClass().toString()) + "):" + value);

        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");

        field.append("<input style='").append(getElementStyle()).append("' class='").append(getStyleClasses()).append("'");
        field.append(" id='").append(fieldName).append("' name='").append(fieldName).append("' value='");
        if (value != null) {
            field.append(value);
        }
        field.append("'/>\n");
        field.append("</div>\n");
        return field.toString();
    }

}
