/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.CommonUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public class HTMLComponent {

    private String tag;
    private String innerText = "";
    private StringBuilder innerHTML = new StringBuilder();
    private final List<HTMLComponent> contents = new ArrayList();

    String voidEL[] = new String[]{
        "area", "base", "br", "col", "embed", "hr", "img", "input",
        "keygen", "link", "meta", "param", "source", "track", "wbr"
    };

    public HTMLComponent(String tag) {
        this(tag, "");
    }

    public HTMLComponent(String tag, String innerText) {
        this.tag = tag;
        voidType = CommonUtils.isStringExistsInArray(voidEL, tag);
        setInnerText(innerText);
    }

    private HTMLComponent rootComponent = null;

    protected void setRootComponent(HTMLComponent root) {
        this.rootComponent = root;
    }

    protected HTMLComponent getRootComponent() {
        return rootComponent;
    }

    public final void setInnerText(String innerText) {
        if (voidType && !innerText.isEmpty()) {
            throw new RuntimeException("Void type element cannot have content");
        }

        this.innerText = innerText;
        redrawRootComponent();
    }

    public void appendChild(HTMLComponent content) {
        if (content == null) {
            throw new NullPointerException();
        }
        content.setRootComponent(this);
        contents.add(content);
        redrawRootComponent();
    }

    public int contentCount() {
        return contents.size();
    }

    @Override
    public String toString() {
        return innerHTML.toString();
    }

    private final Map<String, String> tagAttributes = new HashMap();

    public void setAttribute(String name, String value) {
        tagAttributes.put(name, value);
        redrawRootComponent();
    }

    public String getAttribute(String name) {
        return tagAttributes.get(name);
    }

    public String removeAttribute(String name) {
        String attr = tagAttributes.remove(name);
        redrawRootComponent();
        return attr;
    }

    public void addStyleClass(String cssClassNames) {
        String cssClasses = tagAttributes.getOrDefault("class", "");
        if (cssClasses.isEmpty()) {
            cssClasses = cssClassNames;
        } else {
            for (String check : cssClassNames.split(" ")) {
                if (!CommonUtils.isStringContaintsWholeWord(cssClasses, check) || !check.isEmpty()) {
                    cssClasses += check + " ";
                }
            }
        }
        tagAttributes.put("class", cssClasses);
        redrawRootComponent();
    }

    public void removeStyleClass(String cssClassNames) {
        String cssClasses = tagAttributes.getOrDefault("class", "");
        if (!cssClasses.isEmpty()) {
            for (String check : cssClassNames.split(" ")) {
                if (!CommonUtils.isStringContaintsWholeWord(cssClassNames, check) && !check.isEmpty()) {
                    cssClasses = cssClasses.replace(cssClasses + " ", "");
                    cssClasses = cssClasses.replace(cssClasses, "");
                }
            }
        }
        tagAttributes.put("class", cssClasses);
        redrawRootComponent();
    }
    private boolean voidType = false;

    protected void renderOpenTag(StringBuilder renderer) {
        renderer.append("\n");
        renderer.append("<");
        renderer.append(tag);
        String encloser;
        for (Map.Entry<String, String> entry : tagAttributes.entrySet()) {
            encloser = entry.getValue().indexOf('\"') > 0 ? "'" : "\"";
            renderer.append(" ");
            renderer.append(entry.getKey());
            renderer.append("=");
            renderer.append(encloser);
            renderer.append(entry.getValue());
            renderer.append(encloser);
        }
        if (voidType) {
            renderer.append(" /");
        }
        renderer.append(">");
        renderer.append("\n");
    }

    protected void renderContents(StringBuilder renderer) {
        for (HTMLComponent content : contents) {
            renderer.append(content);
        }
    }

    protected void renderCloseTag(StringBuilder renderer) {
        if (voidType) {
            return;
        }
        renderer.append("\n");
        renderer.append("</");
        renderer.append(tag);
        renderer.append(">");
        renderer.append("\n");
    }

    protected StringBuilder draw() {
        StringBuilder renderer = new StringBuilder();
        renderOpenTag(renderer);
        renderer.append(innerText);
        renderContents(renderer);
        renderCloseTag(renderer);
        return renderer;
    }

    protected void redrawRootComponent() {
        if (rootComponent != null) {
            rootComponent.redrawRootComponent();
            return;
        }
        redraw();
    }

    public void redraw() {
        for (HTMLComponent cnt : contents) {
            cnt.redraw();
        }
        innerHTML = draw();
    }

}
