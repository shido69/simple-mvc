/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public abstract class BasicRenderer implements Renderer {

    private boolean readOnly = false;
    private String classes = "";
    private String label = null;
    private String fieldName = null;
    private String elementStyle = "";
    private Map rowData = null;
    private Object value;

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Map getRowData() {
        return rowData;
    }

    final public void setRowData(Map rowData) {
        this.rowData = rowData;
        if (this.rowData != null) {
            this.value = this.rowData.get("fieldName");
        } else {
            this.value = null;
        }
    }

    public String getElementStyle() {
        return elementStyle;
    }

    public void setElementStyle(String elementStyle) {
        this.elementStyle = elementStyle;
    }

    final public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

    final public void setLabel(String label) {
        this.label = label;
    }

    final public String getLabel() {
        return label;
    }

    public void setStyleClass(String classes) {
        if (!this.classes.contains(classes.trim())) {
            this.classes = classes;
        }
    }

    public String getStyleClasses() {
        return this.classes;
    }

    public void setReadOnly(boolean val) {
        this.readOnly = val;
    }

    public boolean isReadOnly() {
        return this.readOnly;
    }

    @Override
    public abstract String render(String fieldName, String labelText, Map rowData, Object value);

    public String render() {
        return render(this.getFieldName(), this.getLabel(), getRowData(), getValue());
    }
}
