/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.Env;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shido69
 */
public class FileUploadField extends BasicRenderer {

    StringBuilder field = new StringBuilder();

    FileUploadField(String label) {
        setLabel(label);
    }

    @Override
    public String render(String fieldName, String fieldLabel, Map rowData, Object value) {

        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }
        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");
        field.append("<div>\n");
        field.append("<input style='").append(getElementStyle()).append("' class='form-control' id='" + fieldName + "' name='").append(fieldName).append("' type='file'/>\n");
        field.append("</div>");
        field.append("</div>\n");
        return field.toString();
    }

}
