/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.CommonUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class TableBuilder {

    private Map<String, HeaderRenderer> customHeaderRenderer;
    private Map<String, ColumnRenderer> customColRenderer;
    private Map<String, Boolean> hiddenFields;
    private Map header;
    private List<Map> rows;
    private String classes, headerStyleClass, headerColStyleClass, dataRowStyleClass, dataColStyleClass, tabelBodyStyleClass;

    public void setTabelBodyStyleClass(String tabelBodyStyleClass) {
        this.tabelBodyStyleClass = tabelBodyStyleClass;
    }

    public void setDataRowStyleClass(String dataRowStyleClass) {
        this.dataRowStyleClass = dataRowStyleClass;
    }

    public void setDataColStyleClass(String dataColStyleClass) {
        this.dataColStyleClass = dataColStyleClass;
    }

    public void setCustomHeaderRenderer(String fieldName, HeaderRenderer renderer) {
        this.customHeaderRenderer.put(fieldName, renderer);
    }

    public void setCustomColRenderer(String fieldName, ColumnRenderer renderer) {
        this.customColRenderer.put(fieldName, renderer);
    }

    public void setHidden(String fieldName, boolean hidden) {
        this.hiddenFields.put(fieldName, hidden);
    }

    public void setHeaderStyleClass(String headerStyleClass) {
        this.headerStyleClass = headerStyleClass;
    }

    public void setHeaderColStyleClass(String headerColStyleClass) {
        this.headerColStyleClass = headerColStyleClass;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }
    private String id;

    public TableBuilder(String id, List<Map> rows) throws NullPointerException {
        this(id, null, rows);
        Map headers = new LinkedHashMap();
        if (!rows.isEmpty()) {
            Map rowHeader = rows.get(0);
            Set<String> keySet = (Set<String>) rowHeader.keySet();
            for (String key : keySet) {
                headers.put(key, CommonUtils.cleanFieldName(key));
            }
        }
        this.header = headers;
    }

    public TableBuilder(String id, Map headers, List<Map> rows) throws NullPointerException {
        this.customHeaderRenderer = new HashMap();
        this.customColRenderer = new HashMap();
        this.hiddenFields = new HashMap();
        this.id = id;
        this.header = headers;
        this.rows = rows;
    }

    private String cleanFieldName(String fieldName) {
        return CommonUtils.cleanFieldName(fieldName);
    }

    @Override
    public String toString() {
        return render();
    }

    public String render() {
        return render(false);
    }
    private BasicRenderer defaultRenderer = new GenericTextField();

    public void setDefaultRenderer(BasicRenderer defaultRenderer) {
        this.defaultRenderer = defaultRenderer;
    }

    private static final String TABLE_OPEN_TAG = "<table id='%s' class='%s'%s>";
    private static final String TABLE_CLOSE_TAG = "</table>";
    private static final String TABLE_HEADER_OPEN_TAG = "<thead class='%s'>";
    private static final String TABLE_HEADER_CLOSE_TAG = "</thead>";
    private static final String TABLE_BODY_OPEN_TAG = "<tbody class='%s'>";
    private static final String TABLE_BODY_CLOSE_TAG = "</tbody>";
    private static final String TABLE_ROW_OPEN_TAG = "<tr id='row_%s' class='%s'>";
    private static final String TABLE_ROW_CLOSE_TAG = "</tr>";
    private static final String TABLE_COL_OPEN_TAG = "<td id='%s' class='%s'>";
    private static final String TABLE_COL_CLOSE_TAG = "</td>";
    private static final String TABLE_HEADER_COL_OPEN_TAG = "<th id='hdr_%s' class='%s'>";
    private static final String TABLE_HEADER_COL_CLOSE_TAG = "</th>";
    private static Logger logger = Logger.getLogger(TableBuilder.class);

    private boolean isVisible(String fieldName) {
        Boolean hidden = this.hiddenFields.get(fieldName);
        return hidden == null ? true : !hidden;
    }

    protected void renderHeader() {
        table.append(String.format(TABLE_HEADER_OPEN_TAG, this.headerStyleClass));
        table.append(String.format(TABLE_ROW_OPEN_TAG, "table-header", this.headerStyleClass));
        HeaderRenderer renderer;
        for (Map.Entry<String, String> field : (Set<Map.Entry<String, String>>) header.entrySet()) {
            if (!isVisible(field.getKey())) {
                continue;
            }
            renderer = this.customHeaderRenderer.get(field.getKey());
            table.append(String.format(TABLE_HEADER_COL_OPEN_TAG, field.getKey(), this.headerColStyleClass));
            if (renderer != null) {
                table.append(((HeaderRenderer) renderer).render(field.getKey(), field.getValue()));
            } else {
                table.append(field.getValue());
            }
            table.append(TABLE_HEADER_COL_CLOSE_TAG);
        }
        table.append(TABLE_ROW_CLOSE_TAG);
        table.append(TABLE_HEADER_CLOSE_TAG);
    }

    protected void renderBody() {
        table.append(String.format(TABLE_BODY_OPEN_TAG, this.tabelBodyStyleClass));
        int idx = 0;
        logger.info("Render TableBody");
        ColumnRenderer renderer;
        if (rows != null) {
            for (Map row : rows) {
                logger.info("RowData[" + idx + "]:");
                table.append(String.format(TABLE_ROW_OPEN_TAG, ("data_" + idx), this.dataRowStyleClass));
                for (String fieldName : (Set<String>) header.keySet()) {
                    if (!isVisible(fieldName)) {
                        continue;
                    }
                    Object value = row.get(fieldName);
                    logger.info(fieldName + " -> " + value);
                    renderer = this.customColRenderer.get(fieldName);
                    logger.info("Renderer:" + (renderer == null ? "default" : renderer.getClass().getName()));
                    table.append(String.format(TABLE_COL_OPEN_TAG, fieldName, this.dataColStyleClass == null ? "" : this.dataColStyleClass));
                    if (renderer != null) {
                        table.append(renderer.render(idx, row, value));
                    } else {
                        table.append(value == null ? "" : value);
                    }
                    table.append(TABLE_COL_CLOSE_TAG);
                }
                table.append(TABLE_ROW_CLOSE_TAG);
                idx++;
            }
        }
        table.append(TABLE_BODY_CLOSE_TAG);
    }

    private StringBuilder table;

    protected String render(boolean redraw) {
        if (table == null || redraw) {
            logger.debug("Rendering Table" + (redraw ? "(Forced)" : ""));
            table = new StringBuilder();
            int i = 0;
            table.append(String.format(TABLE_OPEN_TAG, this.id, this.classes, renderAttributes()));
            renderHeader();
            renderBody();
            table.append(TABLE_CLOSE_TAG);
        }
        return table.toString();
    }
    private Map<String, String> attrs = new HashMap();

    private String renderAttributes() {
        String result = "";
        for (Map.Entry<String, String> e : attrs.entrySet()) {
            result += " " + e.getKey() + "=\"" + e.getValue().replace('"', '\'') + "\"";
        }
        return result;
    }

    public void setAttribute(String attr, String val) {
        attrs.put(attr, val);
    }
}
