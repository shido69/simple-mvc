/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import com.shido.sfx.util.Env;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public abstract class FKLookupField extends BasicRenderer {

    private String idField = "id", valueField = "value";

    public void setIdField(String idField) {
        this.idField = idField;
    }

    public void setValueField(String valueField) {
        this.valueField = valueField;
    }

    public FKLookupField(String fieldName, String label) {
        setFieldName(fieldName);
        setLabel(label);
    }

    public abstract List<Map> lookup(Object id);

    private static Logger logger = Logger.getLogger(FKLookupField.class);

    @Override
    public String render(String fieldName, String fieldLabel, Map data, Object value) {
        StringBuilder field = new StringBuilder();
        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }

        logger.debug(fieldName + "(" + (value == null ? "Generic" : value.getClass().toString()) + "):" + value);

        field.append("<div class='form-group'>\n");
        field.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");
        field.append("<select style='").append(getElementStyle()).append("' name='").append(fieldName).append("' id='").append(fieldName).append("' class='").append(getStyleClasses()).append("'>");
        field.append("<option value='0'>---</option>");
        for (Map ldata : lookup(value)) {
            Object id = ldata.get(idField);
            Object val = ldata.get(valueField);
            String selected = id.equals(value) ? "selected" : "";
            field.append(String.format("<option value='%1$s' %3$s>%2$s</option>", id.toString(), val.toString(), selected));
        }
        field.append("</select>");
        field.append("</div>\n");
        return field.toString();
    }

    @Override
    public String toString() {
        return render(this.getFieldName(), this.getLabel(), this.getRowData(), this.getValue());
    }

}
