/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class HiddenField extends BasicRenderer {

    HiddenField() {
    }

    private static Logger logger = Logger.getLogger(HiddenField.class);

    @Override
    public String render(String fieldName, String labelText, Map rowData, Object value) {

        String field = String.format("<input type='hidden' id='%1$s' name='%1$s' value='%2$s' %3$s/>",
                fieldName, value != null ? value.toString() : "", isReadOnly() ? "readonly" : "");
        return field;
    }

}
