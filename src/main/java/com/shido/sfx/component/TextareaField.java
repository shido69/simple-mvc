/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.component;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class TextareaField extends BasicRenderer {

    public TextareaField() {
    }

    public TextareaField(String label) {
        setLabel(label);
    }

    private static Logger logger = Logger.getLogger(TextareaField.class);

    @Override
    public String render(String fieldName, String labelText, Map rowData, Object value) {
        StringBuilder form = new StringBuilder();
        String label = getLabel();
        if (label == null) {
            label = fieldName;
        }

        logger.debug(fieldName + "(" + (value == null ? "Generic" : value.getClass().toString()) + "):" + value);

        form.append("<div class='form-group'>\n");
        form.append("<label for='").append(fieldName).append("'>").append(label).append("</label>\n");

        form.append("<textarea style='").append(getElementStyle()).append("' class='").append(this.getStyleClasses()).append("'");
        form.append("' id='" + fieldName + "' name='").append(fieldName).append("'>");
        if (value != null) {
            form.append(value);
        }
        form.append("</textarea>");
        form.append("</div>\n");
        return form.toString();
    }

    

}
