/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.exceptions.PageAccessViolationException;
import com.shido.sfx.exceptions.PageAccessViolationWithRedirectException;

/**
 *
 * @author lenovo
 */
public interface AuthenticationService {
    public void login(String userId, String password);
    public void logout(String userId);
    public void validateSession(String message, String page) throws PageAccessViolationWithRedirectException;
    public boolean hasAccessTo(int role);
    public boolean hasAccessTo(String route);
    public boolean allowInsert(int role);
    public boolean allowEdit(int role);
    public boolean allowDelete(int role);
    public boolean isItemVisibleTo(int check, int role);
}
