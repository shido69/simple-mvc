/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shido69
 */
public interface ApplicationContext extends Map<String, Object> {

    public static final String SQL_DEBUG = "sql_debug";
    public static final String JDBC_CONNECTION = "jdbcConnection";
    public static final String AUTO_COMMIT_CONNECTION = "JDBC_AUTO_COMMIT_CONNECTION";
    public static final String HTTP_REQUEST = "HttpServletRequest";
    public static final String SERVLET_CONFIG = "ServletConfig";
    public static final String HTTP_RESPONSE = "HttpServletResponse";
    public static final String HTTP_SESSION = "HttpSession";
    public static final String SERVLET_CONTEXT = "ServletContext";
    public static final String BASE_URI = "BaseURI";
    public static final String SERVLET_PATH = "ServletPath";
    public static final String CONTEXT_PATH = "ContextPath";
    public static final String PATH_INFO = "PathInfoString";
    public static final String REQUEST_URI = "RequestURI";
    public static final String FORM_REQUEST = "FormRequest";
    public static final String FLASH_STORAGE = "FlashStorage";
    public static final String SYS_ERROR = "sys_error";
    public static final String SYS_ALERT = "sys_alert";
    public static final String SYS_ALERT_TYPE = "sys_alert_type";
    public static final String SYS_ALERT_TITLE = "sys_alert_title";
    public static final String SYS_ALERT_MESSAGE = "sys_alert_message";
    public static final String CURRENT_ACTIVE_USER = "current_active_user";

    public String getId();
    public HttpServletRequest getHttpServletRequest();

    public HttpServletResponse getHttpServletResponse();

    public HttpSession getSession();

    public String getBaseURI();

    public String getContextPath();

    public FormRequests getFormRequest();

    String getPathInfo();

    String getRequestURI();

    public ServletConfig getServletConfig();

    public ServletContext getServletContext();

    public void createAlert(String type, String title, String message);

    public void flashData(String key, Object value);

    public Object getFlashData(String key);

    public String getServletPath();

    public void clearFlashData();

    public HttpSession createNewSession();

    public Map getCurrentActiveUser();

    public void setCurrentActiveUser(Map user);

}
