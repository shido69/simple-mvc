/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author lenovo
 */
public class SimpleJDBCDataSource implements DataSource {

    String settings;
    private static final Logger logger=Logger.getLogger(SimpleJDBCDataSource.class);
    public SimpleJDBCDataSource(String settings) {
        try {
            this.settings = settings.trim().length() > 0 ? "." + settings : "";
            String driver = Env.getConfig().getProperty(String.format(Env.DATABASE_DRIVER_PARAM, this.settings));
            Class.forName(driver).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            logger.error("Line " + e.getStackTrace()[0].getLineNumber() + ":" + e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        String urlParam = String.format(Env.DATABASE_URL_PARAM, this.settings);
        String urlValue = Env.getConfig().getProperty(urlParam);
        String userParam = String.format(Env.DATABASE_USER_PARAM, this.settings);
        String userValue = Env.getConfig().getProperty(userParam);
        String passwordParam = String.format(Env.DATABASE_PASSWORD_PARAM, this.settings);
        String passwordValue = Env.getConfig().getProperty(passwordParam);
        Connection con = DriverManager.getConnection(urlValue,
                userValue,
                passwordValue
        );
        return con;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return DriverManager.getConnection(Env.getConfig().getProperty(String.format(Env.DATABASE_URL_PARAM, "." + settings)),
                username,
                password
        );
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return DriverManager.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        DriverManager.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        DriverManager.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return DriverManager.getLoginTimeout();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        Class[] ifcs = this.getClass().getInterfaces();
        for (Class ifc : ifcs) {
            if (iface == ifc) {
                return (T) this;
            }
        }
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        Class[] ifcs = this.getClass().getInterfaces();
        for (Class ifc : ifcs) {
            if (iface == ifc) {
                return true;
            }
        }
        return false;
    }

    @Override
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return java.util.logging.Logger.getGlobal();
    }

}
