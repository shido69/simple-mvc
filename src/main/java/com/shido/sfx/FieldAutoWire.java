/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class FieldAutoWire {

    public static Field[] findCompatibleFields(Class reflectClass, Class fieldType) {
        List<Field> fields = new ArrayList();
        Field[] theFields = reflectClass.getDeclaredFields();
        for (Field f : theFields) {
            if (f.getType().isAssignableFrom(fieldType)) {
                fields.add(f);
            } else if (f.getType().isPrimitive()) {
                if ((f.getType() == byte.class && (fieldType == byte.class || fieldType == Byte.class))
                        || (f.getType() == short.class && (fieldType == byte.class || fieldType == short.class || fieldType == Byte.class || fieldType == Short.class))
                        || (f.getType() == int.class && (fieldType == byte.class || fieldType == short.class || fieldType == int.class || fieldType == Byte.class || fieldType == Short.class || fieldType == Integer.class))
                        || (f.getType() == float.class && (fieldType == byte.class || fieldType == short.class || fieldType == int.class || fieldType == float.class || fieldType == Byte.class || fieldType == Short.class || fieldType == Integer.class || fieldType == Float.class))
                        || (f.getType() == double.class && (fieldType == byte.class || fieldType == short.class || fieldType == int.class || fieldType == float.class || fieldType == double.class || fieldType == Byte.class || fieldType == Short.class || fieldType == Integer.class || fieldType == Float.class || fieldType == Double.class))) {
                    fields.add(f);
                }
            }
        }
        return fields.toArray(new Field[fields.size()]);
    }
    private static final Logger logger = Logger.getLogger(FieldAutoWire.class);

    public static void setFieldValueByType(Object o, Class type, Object value)  {
        Field[] fs = findCompatibleFields(o.getClass(), type);
        for (Field f : fs) {
            logger.info((f.isAccessible() ? "" : "Non ") + "Accessible Field '" + f.getName() + "' found with type " + f.getType().getName());
            try {
                if (!f.isAccessible()) {
                    f.setAccessible(true);
                }
                logger.info("Old Value = " + f.get(o));
                f.set(o, value);
                logger.info("New Value = " + f.get(o).toString());
            } catch (IllegalArgumentException ex) {
                logger.warn("Illegal Argument: Incompatible types",ex);
            } catch (IllegalAccessException ex) {
                logger.warn("Illegal Access: Field is private",ex);
            }
        }
    }

}
