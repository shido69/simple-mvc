/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.Logger;

/**
 *
 * @author Shido69
 */
public class Scheduler {

    private static Logger logger = Logger.getLogger(Scheduler.class);
    private Timer timer;
    private static List<Map> taskList;

    public Scheduler() {
        loadSchedule(Env.getConfig());
    }
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    DateFormat df = DateFormat.getDateTimeInstance();

    public void reloadSchedule() {
        destroy();
        loadSchedule(Env.getConfig());
    }

    private void loadSchedule(Properties conf) {

        String tasks = conf.getProperty("sfx.scheduler.tasks", "SFX_SCHEDULER");
        logger.info("tasks:" + tasks);
        if (tasks.isEmpty()) {
            return;
        }
        if (taskList == null) {
            taskList = new ArrayList();
        }

        logger.info("===Loading Scheduled Task===");
        Calendar now = Calendar.getInstance();
        String arrTasks[] = tasks.trim().split(",");
        for (String task : arrTasks) {
            logger.info("--==Processing " + task + "==--");
            Object classOject = null;
            String handlerClass = conf.getProperty(task + ".class");
            try {
                classOject = Class.forName(handlerClass).newInstance();
            } catch (NullPointerException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                logger.debug("cannot create instance of class " + conf.getProperty(task + ".class"), e);
            }
            logger.info("Default TaskHandler Object [" + handlerClass + "] is " + (classOject != null));
            String types = conf.getProperty(task + ".type", "");
            String[] arrTypes = types.split(",");

            for (String type : arrTypes) {
                try {
                    logger.info("Type:'" + type + "'");

                    String timeTexts = conf.getProperty(task + "." + type);

                    String[] arrTimeTexts = timeTexts.split(";");
                    for (String timeText : arrTimeTexts) {
                        Calendar startTime = Calendar.getInstance();
                        if (type.equalsIgnoreCase("interval")) {
                            int arg = Integer.parseInt(timeText);
                            startTime.add(Calendar.MINUTE, arg);
                        } else if (type.equalsIgnoreCase("daily")) {
                            Date parseTime = sdf.parse(timeText);
                            startTime.setTime(parseTime);
                            startTime.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE));
                            if (startTime.compareTo(now) < 0) {
                                startTime.add(Calendar.DATE, 1);
                            }
                        } else {
                            String[] args = timeText.split(",");
                            Date arg1 = sdf.parse(args[1]);
                            startTime.setTime(arg1);
                            startTime.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE));
                            Integer arg2 = Integer.parseInt(args[0]);
                            if (type.equalsIgnoreCase("weekly")) {
                                int dow = now.get(Calendar.DAY_OF_WEEK);
                                logger.info("Current Date:" + startTime.getTime().toString());
                                boolean check = now.get(Calendar.DAY_OF_WEEK) > arg2;
                                if (check) {
                                    startTime.add(Calendar.DATE, 7 - (dow - arg2));
                                    logger.info("Next Week @:" + startTime.getTime().toString());
                                } else {
                                    startTime.add(Calendar.DATE, arg2 - dow);
                                    logger.info("Current Week @:" + startTime.getTime().toString());
                                }
                            } else if (type.equalsIgnoreCase("monthly")) {
                                startTime.set(Calendar.DATE, arg2);
                                if (startTime.compareTo(now) < 0) {
                                    startTime.add(Calendar.MONTH, 1);
                                }
                            }
                        }
                        logger.info(String.format("TimeText: %s -> %s", timeText, df.format(startTime.getTime())));

                        Map detail = new HashMap();
                        detail.put("config_key", task + "." + type);
                        detail.put("type", type);
                        detail.put("start_time", startTime);
                        detail.put("class", classOject);
                        detail.put("last_execution_time", null);
                        detail.put("is_running", Boolean.FALSE);

                        String cls = conf.getProperty(task + "." + type + ".class", "");
                        if (!cls.isEmpty()) {
                            try {
                                detail.put("class", Class.forName(cls).newInstance());
                            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                                logger.error("error create instance of class " + cls, e);
                            }
                        }
                        taskList.add(detail);
                    }
                } catch (Exception e) {
                    logger.debug("scheduled taks configuration skipped!");
                }
            }
        }
        if (taskList != null) {
            String taskerName = Env.getConfig().getProperty("sfx.scheduler.name", "SFX_TASKER");
            logger.info("--Init Timer [" + taskerName + "]--");
            timer = new Timer(taskerName, true);
            TimerTask tt = new TimerTask() {
                @Override
                public void run() {
                    runTask();
                }
            };
            timer.scheduleAtFixedRate(tt, 0, Long.parseLong(Env.getConfig().getProperty("sfx.scheduler.interval", "60000")));
        }
    }

    private void runTask() {
        logger.debug("Running task: " + Thread.currentThread().getName());
        ApplicationContext context = new ApplicationContextImpl();
        Env.setContext(context);
        Calendar now = Calendar.getInstance();
        int executed = 0;
        for (Map task : taskList) {
            Calendar exeTime = (Calendar) task.get("start_time");
            String type = (String) task.get("type");
            if (exeTime.compareTo(now) <= 0) {
                if ((Boolean) task.get("is_running")) {
                    logger.info("Previous task still running...");
                    continue;
                }
                logger.info(String.format("Executing %s task at %s", type, df.format(now.getTime())));
                Runnable run = (Runnable) task.get("class");
                task.put("is_running", Boolean.TRUE);
                try {
                    run.run();
                } catch (Exception e) {
                    logger.error(e);
                }
                task.put("is_running", Boolean.FALSE);
                task.put("last_execution_time", exeTime);

                if (type.equalsIgnoreCase("interval")) {
                    int interval = Integer.parseInt(Env.getConfig().getProperty((String) task.get("config_key"), "0"));
                    if (interval > 0) {
                        exeTime.add(Calendar.MINUTE, executed);
                    } else {
                        taskList.remove(task);
                    }
                } else if (type.equalsIgnoreCase("daily")) {
                    exeTime.add(Calendar.DATE, 1);
                } else {
                    if (type.equalsIgnoreCase("weekly")) {
                        exeTime.add(Calendar.DATE, 7);
                    } else if (type.equalsIgnoreCase("monthly")) {
                        exeTime.add(Calendar.MONTH, 1);
                    }
                }
                logger.info(String.format("Next Execution %s", df.format(exeTime.getTime())));
                task.put("start_time", exeTime);
                executed++;
            }
        }
        logger.debug("Executed task:" + executed + " of " + taskList.size());
    }

    private void destroy() {
        if (timer != null) {
            timer.cancel();
            int x = timer.purge();
            logger.info(x + " task purged");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("destroy()");
        }
    }

}
