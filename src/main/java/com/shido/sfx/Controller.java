/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.annotations.Request;
import com.shido.sfx.annotations.Response;
import com.shido.sfx.annotations.Route;
import com.shido.sfx.annotations.Template;
import com.shido.sfx.annotations.ValidateSession;
import com.shido.sfx.exceptions.PageAccessViolationWithRedirectException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;

import com.shido.sfx.exceptions.InvalidControllerCallException;
import com.shido.sfx.exceptions.InvalidControllerMethodCallException;
import com.shido.sfx.exceptions.URLFilteredExeptions;
import com.shido.sfx.util.CommonUtils;
import com.shido.sfx.util.Env;
import com.shido.sfx.util.HttpUtils;
import java.lang.reflect.Field;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 *
 * @author Shido69
 */
public class Controller {

    static Object invoke(String scanLoc, String mvcCallPath) throws Throwable {
        HttpServletRequest request = Env.getContext().getHttpServletRequest();
        HttpServletResponse response = Env.getContext().getHttpServletResponse();
        ServletConfig config = Env.getContext().getServletConfig();

        Class ctrl = null;
        Method route = null;
        Object[] params = null;
        Class[] controllers = null;
//        controllers = com.shido.sfx.util.ClassScanner.getSFXControllerClass(scanLoc);
        controllers = com.shido.sfx.util.ClassScanner.getSFXControllerClass(scanLoc);
        if (controllers != null) {
            for (Class c : controllers) {
                route = com.shido.sfx.util.ClassScanner.getSFXMethod(c, mvcCallPath, request.getMethod());
                if (route != null) {
                    ctrl = c;
                    params = com.shido.sfx.util.ClassScanner.getParameterValues(route, mvcCallPath);
                    break;
                }
            }
            logger.info(route);
        }

        Object result = null;
        if (ctrl != null && route != null) {
            try {
                Object instance = ctrl.newInstance();
                autoWire(instance, request, response, config);
                Route routeAnnot = route.getAnnotation(Route.class);
                if (routeAnnot.requireAuth()) {
                    String auth = request.getHeader("Authorization");
                    boolean isAuth = HttpUtils.authenticate(auth, routeAnnot.authUID(), routeAnnot.authPassword());
                    logger.info("Autentication " + (isAuth ? "Success" : "Failed"));
                    if (!isAuth) {
                        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                        return null;
                    }
                }
                ValidateSession valSes = route.getAnnotation(ValidateSession.class);
                if (valSes != null) {
                    AuthenticationService authService = ServiceFactory.getAuthenticationService();
                    authService.validateSession(valSes.message(), valSes.redirectUrl());
                }
                result = route.invoke(instance, params);
                request.setAttribute("current", instance);
//                request.setAttribute("this", instance);
                request.setAttribute(ctrl.getName(), instance);
            } catch (Exception e) {
                if (e.getCause() != null) {
                    throw e.getCause();
                } else {
                    throw new InvalidControllerMethodCallException(e);
                }
            }
        } else {
            throw new InvalidControllerCallException("No routing found for '" + mvcCallPath + "'");
        }

        request.setAttribute("sfx_controller_result", result);
        if (route.isAnnotationPresent(Template.class)) {
            Template template = route.getAnnotation(Template.class);
            result = CommonUtils.createTemplate(
                    template.base(),
                    template.title(),
                    template.subContent(),
                    template.sitePath(),
                    template.contentPath().isEmpty() && result != null ? result.toString() : template.contentPath(),
                    template.additionalHeader(),
                    template.additionalFooter());
            result = new ResponseView((String) result);
        } else if (result != null) {
            if (result instanceof String && ((String) result).endsWith(".jsp")) {
                result = new ResponseView((String) result);
            } else {
                result = new ResponseBody(result.toString());
            }
        }

        return result;
    }

    private static void autoWire(Object instance, HttpServletRequest request, HttpServletResponse response, ServletConfig config) throws IllegalArgumentException, IllegalAccessException {
        Class c = instance.getClass();
        for (Field f : c.getDeclaredFields()) {
            if (f.isAnnotationPresent(Request.class)) {
                f.setAccessible(true);
                f.set(instance, request);
            }
            if (f.isAnnotationPresent(Response.class)) {
                f.setAccessible(true);
                f.set(instance, response);
            }
            if (f.isAnnotationPresent(com.shido.sfx.annotations.ServletConfig.class)) {
                f.setAccessible(true);
                f.set(instance, config);
            }

        }
    }

    private Object controller;
    String[] signal;

    public Object getObject() {
        return this.controller;
    }

    public String getName() {
        return this.controller.getClass().getSimpleName();
    }

    public Controller(String pathInfo) throws InvalidControllerCallException {
        this(Controller.class.getPackage().toString(), pathInfo);
    }

    public Controller(String searchPackage, String pathInfo) throws InvalidControllerCallException, URLFilteredExeptions {

        String[] pathInfos = pathInfo.substring(1).split("/");
        parseSignal(pathInfos);
        try {
            logger.info("searchPackage:" + searchPackage + ", pathInfo:" + pathInfo);
            this.controller = getControllerClass(searchPackage, pathInfos[0]).newInstance();
        } catch (ClassNotFoundException ex) {
            throw new InvalidControllerCallException(ex);
        } catch (IOException ex) {
            throw new InvalidControllerCallException(ex);
        } catch (InstantiationException ex) {
            throw new InvalidControllerCallException(ex.getCause());
        } catch (IllegalAccessException ex) {
            throw new InvalidControllerCallException(ex);
        } catch (NullPointerException ex) {
            throw new InvalidControllerCallException(ex);
        }

    }

    private void parseSignal(String... signal) throws URLFilteredExeptions {
        this.signal = new String[signal.length - 1];
        for (int i = 1; i < signal.length; i++) {
            this.signal[i - 1] = signal[i];
        }
    }

    private Class getControllerClass(String searchPackage, String controllerClassName) throws ClassNotFoundException, IOException {
        Class result = null;
        Class classes[] = ClassScanner.getClasses(searchPackage);
        for (Class klass : classes) {
            if (klass.getName().equalsIgnoreCase(searchPackage + "." + controllerClassName) || klass.getName().equalsIgnoreCase(searchPackage + "." + controllerClassName + "Controller")) {
                result = klass;
                break;
            }
        }
        return result;
    }

    public Object callMethod() throws InvalidControllerMethodCallException {
        return this.callMethod((Object[]) this.signal);
    }
    private static Logger logger = Logger.getLogger(Controller.class);

    public Object callMethod(Object... signal) throws InvalidControllerMethodCallException {
        Object result = null;
        if (this.controller == null) {
            throw new InvalidControllerMethodCallException(new NullPointerException());
        }
        if (signal.length == 0) {
            signal = new Object[]{"index"};
        }
        Method method = MethodScanner.findCompatibleMethod(this.controller.getClass(), signal);
        if (method == null) {
            throw new InvalidControllerMethodCallException("method not found: " + signal[0]);
        }
        logger.debug("Method Name: " + method.getName());
        Class types[] = method.getParameterTypes();
        String info = "MethodParam type=[";
        for (Class c : types) {
            info += c.getName() + ", ";
        }
        try {
            info = info.substring(0, info.lastIndexOf(", ")) + "]";
        } catch (IndexOutOfBoundsException e) {
            info += "]";
        }
        logger.debug(info);
        Object[] params;
        try {
            params = MethodScanner.getCompatibleParam(types, signal);
            info = "Signal type=[";
            for (Object p : params) {
                info += p == null ? null : p.getClass().getName() + ", ";
            }
            try {
                info = info.substring(0, info.lastIndexOf(", ")) + "]";
            } catch (IndexOutOfBoundsException e) {
                info += "]";
            }
            logger.debug(info);
            result = method.invoke(this.controller, params);
        } catch (IllegalAccessException ex) {
            throw new InvalidControllerMethodCallException(ex);
        } catch (IllegalArgumentException ex) {
            throw new InvalidControllerMethodCallException(ex);
        } catch (InvocationTargetException ex) {
            throw new InvalidControllerMethodCallException(ex.getCause());
        } catch (NullPointerException ex) {
            throw new InvalidControllerMethodCallException(ex);
        }
//        System.out.println("Return: " + result);
        return result;
    }

    public void setFieldByValueType(Object object) {
        FieldAutoWire.setFieldValueByType(this.controller, object.getClass(), object);
    }
}
