/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.exceptions;

/**
 *
 * @author Shido69
 */
public class PageAccessViolationWithRedirectException extends PageAccessViolationException {
    private String redirectURL;

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }
    
    public PageAccessViolationWithRedirectException() {
        super();
    }

    public PageAccessViolationWithRedirectException(String message) {
        super(message);
    }
    public PageAccessViolationWithRedirectException(String message,String redirectURL) {
        super(message);
        this.redirectURL=redirectURL;
    }

    public PageAccessViolationWithRedirectException(Throwable throwable) {
        super(throwable);
    }
    public PageAccessViolationWithRedirectException(String message,Throwable throwable) {
        super(message, throwable);
    }
    
}
