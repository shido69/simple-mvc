/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.exceptions;

/**
 *
 * @author Shido69
 */
public class URLFilteredExeptions extends RuntimeException {


    

    public URLFilteredExeptions() {
        super();
    }

    public URLFilteredExeptions(String message) {
        super(message);
    }

    public URLFilteredExeptions(Throwable throwable) {
        super(throwable);
    }
}
