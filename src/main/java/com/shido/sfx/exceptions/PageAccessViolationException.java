/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.exceptions;

/**
 *
 * @author Shido69
 */
public class PageAccessViolationException extends RuntimeException {

    public PageAccessViolationException() {
        super();
    }

    public PageAccessViolationException(String message) {
        super(message);
    }

    public PageAccessViolationException(Throwable throwable) {
        super(throwable);
    }
    public PageAccessViolationException(String message,Throwable throwable) {
        super(message, throwable);
    }
    
}
