/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.exceptions;

/**
 *
 * @author Shido69
 */
public class InvalidControllerCallException extends RuntimeException {

    public InvalidControllerCallException() {
        super();
    }

    public InvalidControllerCallException(String message) {
        super(message);
    }

    public InvalidControllerCallException(Throwable throwable) {
        super(throwable);
    }

}
