/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.exceptions;

/**
 *
 * @author Shido69
 */
public class InvalidControllerMethodCallException extends RuntimeException {

    public InvalidControllerMethodCallException() {
        super();
    }

    public InvalidControllerMethodCallException(String message) {
        super(message);
    }

    public InvalidControllerMethodCallException(Throwable throwable) {
        super(throwable);
    }
}
