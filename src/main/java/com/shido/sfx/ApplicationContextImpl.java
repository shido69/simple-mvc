/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import org.apache.log4j.Logger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shido69
 */
public class ApplicationContextImpl implements ApplicationContext {

    private Map<String, Object> internalMap;

    public String getId(){
        return (String) internalMap.get("id");
    }
    public ApplicationContextImpl() {
        this.internalMap = new HashMap();
    }

    public String getRequestURI() {
        return (String) internalMap.get(REQUEST_URI);
    }

    public String getBaseURI() {
        return (String) internalMap.get(BASE_URI);
    }

    public String getContextPath() {
        return (String) internalMap.get(CONTEXT_PATH);
    }

    public String getPathInfo() {
        return (String) internalMap.get(PATH_INFO);
    }

    private static Logger logger = Logger.getLogger(ApplicationContextImpl.class);

    public FormRequests getFormRequest() {
        logger.info("Parsing Request...");
        FormRequests formRequest = (FormRequests) internalMap.get(FORM_REQUEST);
        if (formRequest == null) {
            try {
                formRequest = new FormRequests(getHttpServletRequest());
                logger.info(formRequest.toString());
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return formRequest;
    }

    @Override
    public HttpServletRequest getHttpServletRequest() {
        return (HttpServletRequest) this.internalMap.get(HTTP_REQUEST);
    }

    @Override
    public HttpServletResponse getHttpServletResponse() {
        return (HttpServletResponse) this.internalMap.get(HTTP_RESPONSE);
    }

    @Override
    public HttpSession getSession() {
        return (HttpSession) getHttpServletRequest().getSession();
    }

    @Override
    public int size() {
        return internalMap.size();
    }

    @Override
    public boolean isEmpty() {
        return internalMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return internalMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return internalMap.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return internalMap.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return internalMap.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        return internalMap.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        internalMap.putAll(m);

    }

    @Override
    public void clear() {
        internalMap.clear();
    }

    @Override
    public Set<String> keySet() {
        return internalMap.keySet();
    }

    @Override
    public Collection<Object> values() {
        return internalMap.values();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return internalMap.entrySet();
    }

    @Override
    public ServletConfig getServletConfig() {
        return (ServletConfig) internalMap.get(SERVLET_CONFIG);
    }

    @Override
    public ServletContext getServletContext() {
        return (ServletContext) internalMap.get(SERVLET_CONTEXT);
    }

    @Override
    public String getServletPath() {
        return (String) internalMap.get(SERVLET_PATH);
    }

    @Override
    public void flashData(String key, Object value) {
        getHttpServletRequest().setAttribute(key, value);
        Map<String, Object> flash = (Map) getSession().getAttribute(ApplicationContext.FLASH_STORAGE);
        if (flash == null) {
            flash = new HashMap();
            getSession().setAttribute(ApplicationContext.FLASH_STORAGE, flash);
        }
        flash.put(key, value);
    }

    @Override
    public Object getFlashData(String key) {
        Object result = null;
        Map<String, Object> flash = (Map) getSession().getAttribute(ApplicationContext.FLASH_STORAGE);
        if (flash != null) {
            result = flash.remove(key);
        }
        return result;
    }

    @Override
    public void clearFlashData() {
        getSession().removeAttribute(ApplicationContext.FLASH_STORAGE);
    }

    @Override
    public void createAlert(String type, String title, String message) {
        flashData(ApplicationContext.SYS_ALERT, true);
        flashData(ApplicationContext.SYS_ALERT_TYPE, type);
        flashData(ApplicationContext.SYS_ALERT_TITLE, title);
        flashData(ApplicationContext.SYS_ALERT_MESSAGE, message);
    }

    @Override
    public HttpSession createNewSession() {
        HttpSession session = null;
        this.internalMap.put(HTTP_SESSION, session = getHttpServletRequest().getSession(true));
        return session;
    }

    @Override
    public void setCurrentActiveUser(Map user) {
        getSession().setAttribute(ApplicationContext.CURRENT_ACTIVE_USER, user);
    }

    @Override
    public Map getCurrentActiveUser() {
        return (Map) getSession().getAttribute(CURRENT_ACTIVE_USER);
    }

}
