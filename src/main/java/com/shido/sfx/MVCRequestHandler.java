/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import com.shido.sfx.exceptions.PageAccessViolationException;
import com.shido.sfx.exceptions.PageAccessViolationWithRedirectException;
import com.shido.sfx.exceptions.InvalidControllerCallException;
import com.shido.sfx.exceptions.InvalidControllerMethodCallException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Shido69
 */
@WebServlet(
        name = "MVCServletHandler",
        initParams = {
            @WebInitParam(
                    name = "router_config_file",
                    value = "router.properties"
            ),
            @WebInitParam(name = "configuration_file", value = "config.properties"),
            @WebInitParam(name = "log_configuration_file", value = "config_log4j.properties")

        },
        urlPatterns = {"*.html"}
)
public class MVCRequestHandler extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger logger = Logger.getLogger(MVCRequestHandler.class);
    private static final String LOG_CONFIG_INIT_PARAMETER = "log_configuration_file";
    private static final String CONFIG_INIT_PARAMETER = "configuration_file";

    public boolean checkForLog4jConfigFile() {

        org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
        Enumeration appenders = rootLogger.getAllAppenders();
        return appenders.hasMoreElements();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        if (!checkForLog4jConfigFile()) {
            String logProperties = "/WEB-INF/" + getServletConfig().getInitParameter(LOG_CONFIG_INIT_PARAMETER);
            String logConfigFile = getServletContext().getInitParameter("log-config-file");
            logConfigFile = logConfigFile == null ? "" : "/WEB-INF/" + logConfigFile;
            File fLogProperties = new File(logProperties = config.getServletContext().getRealPath(logConfigFile.isEmpty() ? logProperties : logConfigFile));
//            logger.debug(logProperties);

            if (fLogProperties.exists()) {
                PropertyConfigurator
                        .configure(logProperties);
            } else {
                logger.debug("Config file not found! Using console appender!");
                ConsoleAppender console = new ConsoleAppender();
                String PATTERN = "%d %p[%C{1} %L] %m%n";
                console.setLayout(new PatternLayout(PATTERN));
                console.setThreshold(Level.INFO);
                console.setName("console");
                console.activateOptions();
                Logger.getRootLogger().addAppender(console);
            }
        }
        if (Env.getConfig() == null) {
            String configFile = getServletContext().getInitParameter("config-file");
            configFile = configFile == null ? "" : configFile;
            String configLocation = "/WEB-INF/" + (!configFile.isEmpty() ? configFile : getServletConfig().getInitParameter(CONFIG_INIT_PARAMETER));
//        logger.debug(configLocation);
            logger.info("Loading config " + configLocation);
            try {
                loadMVCProperties(config.getServletContext().getResourceAsStream(configLocation));
            } catch (IOException | NullPointerException ex) {
                logger.debug(ex);
                logger.error("No Config File! Find config using Application interface...");
                Env.setConfig(new Properties());
                try {
                    String classpath = config.getServletContext().getRealPath("/WEB-INF/classes");
                    Class[] apps = ClassScanner.getClasses(new File(classpath));
                    for (Class app : apps) {
                        if (Configuration.class.isAssignableFrom(app)) {
                            logger.debug("config found! " + app.getName());
                            ((Configuration) app.newInstance()).init(config);
                            break;
                        }
                    }
                } catch (Exception e) {
                    logger.error(e);
                }
            }
            Scheduler scheduler = new Scheduler();
            Env.getCache().put(Env.SFX_SCHEDULER_OBJECT, scheduler);
        }
        logger.info("Config:\n" + Env.getConfig());
    }

    protected void loadMVCProperties(InputStream in) throws IOException {
        if (Env.getConfig() == null) {
            Env.setConfig(new Properties());
            Env.getConfig().load(in);
        }
        logger.info("Loaded Config:\n" + Env.getConfig());
    }

    private String mvcCallPath;

    private void flashData(HttpServletRequest request) {
        Map<String, Object> flash = (Map) request.getSession().getAttribute(ApplicationContext.FLASH_STORAGE);
        if (flash != null) {
            for (String key : flash.keySet()) {
                logger.info("Flashing " + key + " => " + flash.get(key));
                request.setAttribute(key, flash.get(key));
            }
        }
    }

    private void initApplicationContext(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Env.init(request, response, getServletConfig());

        mvcCallPath = Env.getMvcCallPath();

        flashData(request);
    }

    private void clearContext() {
        try {
            Env.getContext().clearFlashData();
            Env.clearContext();
        } catch (Exception e) {
            logger.info("Function ignored: Context Already Cleared!");
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("===NEW MVC REQUEST===");
        initApplicationContext(request, response);
        try {
            String view = null;
            String loc = Env.getConfig().getProperty(Env.CONFIG_VIEW_LOCATION, "/WEB-INF");
            Object result = "";
            if (!mvcCallPath.startsWith(loc) && !mvcCallPath.endsWith(".jsp")) {

                String scanLoc = Env.getConfig().getProperty(Env.CONFIG_CONTROLLER_PACKAGE);
                try {
                    result = Controller.invoke(scanLoc, mvcCallPath);
                    if (result != null && !response.isCommitted()) {
                        if (result instanceof ResponseView) {
                            view = loc + "/" + ((ResponseView) result).getViewLocation();
                        } else if (result instanceof ResponseBody) {
                            printControllerResult(((ResponseBody) result).getResponseText());
                        }
                    }
                } catch (InvalidControllerCallException | InvalidControllerMethodCallException e) {
                    if (Boolean.valueOf(Env.getConfig().getProperty("mvc.only_use_new_method", "false"))) {
                        throw e;
                    }
                    logger.warn("New method fail, probing using old method...");

                    if (mvcCallPath.equalsIgnoreCase("/")) {
                        mvcCallPath = Env.getConfig().getProperty(Env.DEFAULT_CONTROLLER_PATH, "/index");
                    }

                    Controller controller = new Controller(scanLoc, mvcCallPath);
                    logger.info("Controller found: " + controller.getName());
                    request.setAttribute("current", controller.getObject());
                    request.setAttribute("this", controller.getObject());
                    logger.info("Set controller attribut to: " + controller.getName());
                    request.setAttribute(controller.getName(), controller.getObject());
                    result = controller.callMethod();
                    if (result != null) {
                        if (result instanceof String) {
                            view = loc + "/" + result + ".jsp";
                        } else {
                            printControllerResult(result);
                        }
                    }

                }

            }
            if (view != null) {
                String realPath = request.getServletContext().getRealPath(view);
                boolean check = false;
                try {
                    File fcheck = new File(realPath);
                    check = fcheck.exists();
                } catch (NullPointerException e) {
                }
                if (check) {
                    logger.info("View: " + view);
                    RequestDispatcher dispatcher = request.getRequestDispatcher(view);
                    if (request.getDispatcherType().compareTo(DispatcherType.REQUEST) == 0) {
                        logger.info("RequestDispatchType detected: using forward method [" + view + "]");
                        dispatcher.forward(request, response);
                        response.getWriter().close();
                        clearContext();
                    } else {
                        logger.info("Non RequestDipatchType detected: using include method [" + view + "]");
                        dispatcher.include(request, response);
                    }
                } else {
                    logger.info("View: Not a valid JSP view, fall back to PLAIN TEXT");
                    printControllerResult(result);
                }
            }
            if (!Env.isAutoCommit()) {
                try {
                    Env.commitTransaction();
                } catch (SQLException ex) {
                    logger.error(ex);
                }
            }
        } catch (Throwable t) {
            handleError(mvcCallPath, response, t);
        }
    }

    private void handleError(String pathInfo, HttpServletResponse response, Throwable t) throws IOException {
        Throwable e = t.getCause();
        if (e == null) {
            e = t;
        }
        if (!Env.isAutoCommit()) {
            try {
                Env.rollbackTransaction();
            } catch (SQLException ex) {
                logger.error(pathInfo, ex);
            }
        }
        if (e instanceof PageAccessViolationException) {
            if (e instanceof PageAccessViolationWithRedirectException) {
                PageAccessViolationWithRedirectException error = (PageAccessViolationWithRedirectException) e.getCause();
                if (error == null) {
                    error = (PageAccessViolationWithRedirectException) e;
                }
                logger.error(pathInfo + ": " + error.getMessage());
                if (error.getRedirectURL() != null) {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    response.sendRedirect(error.getRedirectURL());
                } else {
                    response.sendError(403, "Stop! Authentication Required!");
                }
                Env.getContext().flashData("error", error);
            } else {
                logger.error(pathInfo + ": " + "Authentication Required!");
                response.setHeader("WWW-Authenticate", "BASIC realm=\"empress callback\"");
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } else {
            StackTraceElement se = e.getStackTrace()[0];
            logger.error("PathInfo:"+pathInfo, e);
            response.sendError(404, "Invalid Controller call!");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Empress MVC Request Handler";
    }// </editor-fold>

    private void printControllerResult(Object result) throws IOException {
        PrintWriter out = Env.getContext().getHttpServletResponse().getWriter();
        out.print(result.toString());

    }

}
