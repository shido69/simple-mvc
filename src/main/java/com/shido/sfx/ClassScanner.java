/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 *
 * @author Amaran Sidhiq
 */
public class ClassScanner {

    /**
     * Scans all classes accessible from the context class loader which belong
     * to the given package and subpackages.
     *
     * @param root The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Class[] getClasses(File root)
            throws ClassNotFoundException, IOException {
        File[] dirs = root.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
//                return dir.isDirectory() || name.endsWith(".class");
                File test = new File(dir, name);
                return test.isDirectory() || (!(name.indexOf("$")>0) && name.endsWith(".class"));
            }
        });
        ArrayList<Class> classes = new ArrayList<>();
        String packageName = "";
        for (File file : dirs) {
            if (file.isDirectory()) {
                if (!packageName.isEmpty()) {
                    packageName += ".";
                }
                packageName += file.getName();
                classes.addAll(findClasses(file, packageName));
                continue;
            }
            classes.add(Class.forName(file.getName().substring(0, file.getName().lastIndexOf(".class"))));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    public static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and
     * subdirs.
     *
     * @param directory The base directory
     * @param packageName The package name for classes found inside the base
     * directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (!(file.getName().indexOf("$")>0) && file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

    static Object forName(String property, String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
