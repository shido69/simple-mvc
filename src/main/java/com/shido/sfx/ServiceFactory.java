/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.DBUtils;
import com.shido.sfx.util.Env;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author lenovo
 */
public final class ServiceFactory {

    private static final Logger logger = Logger.getLogger(ServiceFactory.class);

    public static Object getService(String name) {
        Object result = Env.getCache().get(name);
        try {
            Class c = Class.forName(Env.getConfig().getProperty(name));
            Env.getCache().put(name, result = c.newInstance());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            logger.debug(e);
        }
        return result;
    }

    public static AuthenticationService getAuthenticationService() {
        return (AuthenticationService) getService("AuthenticationService");
    }

    public static DataSource getDataSource() {
        String[] settings = Env.getConfig().getProperty(Env.DATABASE_SETTING_PARAM, "").split(",");
        if (settings.length > 0) {
            return getDataSource(settings[0]);
        }
        return getDataSource("");
    }

    public static DataSource[] getDataSources() {
        String[] settings = Env.getConfig().getProperty(Env.DATABASE_SETTING_PARAM, "").split(",");
        List<DataSource> result = new ArrayList();
        for (String setting : settings) {
            result.add(getDataSource(setting));
        }
        result.add(getDataSource(""));

        return result.toArray(new DataSource[result.size()]);
    }
    private static final Map<String, DataSource> dataSourcesMap = new HashMap();

    public static DataSource getDataSource(String settings) {
        if (dataSourcesMap.get(settings) == null) {
            DataSource dataSource = new SimpleJDBCDataSource(settings);
            dataSourcesMap.put(settings, dataSource);
        }
        return dataSourcesMap.get(settings);
    }
}
