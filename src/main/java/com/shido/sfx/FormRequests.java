/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import java.io.File;
import java.io.FileFilter;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.FileCleanerCleanup;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileCleaningTracker;

/**
 *
 * @author Amaran Sidhiq
 */
public class FormRequests implements Map, Serializable {

    @Override
    @SuppressWarnings("FinalizeDeclaration")
    protected void finalize() throws Throwable {
        super.finalize();
        intermap.clear();
        intermap = null;
    }

    public static class UploadProgressListener implements ProgressListener {

        private long bytesRead, contentLenght;
        private int item;

        public long getBytesRead() {
            return bytesRead;
        }

        public void setBytesRead(long bytesRead) {
            this.bytesRead = bytesRead;
        }

        public long getContentLenght() {
            return contentLenght;
        }

        public void setContentLenght(long contentLenght) {
            this.contentLenght = contentLenght;
        }

        public int getItem() {
            return item;
        }

        @Override
        public void update(long pBytesRead, long pContentLength, int pItems) {
            bytesRead = pBytesRead;
            contentLenght = pBytesRead;
            item = pItems;
        }
    }

    public FormRequests(HttpServletRequest request) throws Exception {
        this(request, null);
    }
    boolean multipart;

    public boolean isMultipart() {
        return this.multipart;
    }

    public void setMultipart(boolean multipart) {
        this.multipart = multipart;
    }

    public FormRequests(HttpServletRequest request, File tempFileLocation) throws Exception {
        this.multipart = ServletFileUpload.isMultipartContent(request);
        // Create a factory for disk-based file items
        if (this.multipart) {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            FileCleaningTracker fileCleaningTracker = FileCleanerCleanup.getFileCleaningTracker(request.getSession().getServletContext());

            factory.setFileCleaningTracker(fileCleaningTracker);
            // Set factory constraints
            factory.setSizeThreshold(new Integer(Env.getConfig().getProperty(Env.FILE_SIZE_THRESHOLD, "" + (1024* 1024 * 50))));
            factory.setRepository(tempFileLocation);

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            // Set overall request size constraint
            upload.setSizeMax(new Long(Env.getConfig().getProperty(Env.MAX_FILE_SIZE, "" + (1024 * 1000 * 5))));

            UploadProgressListener listener =(UploadProgressListener) Env.getCache().get("UPLOAD_LISTENER_"+Env.getContext().getId());
            if (listener== null) {
                listener = new UploadProgressListener();
                upload.setProgressListener(listener);
                Env.getCache().put("UPLOAD_LISTENTER_"+Env.getContext().getId(), listener);
            }

            // Parse the request
            List /* FileItem */ items = upload.parseRequest(request);
            for (Iterator it = items.iterator(); it.hasNext();) {
                FileItem item = (FileItem) it.next();
                if (item.isFormField()) {
                    processFormField(item);
                } else {
                    processUploadedFile(item);
                }
            }
        } else {
            //cleanup single array
            intermap = cleanUpSingleArray(request.getParameterMap());
        }
    }

    private Map cleanUpSingleArray(Map map) {
        Map m = new HashMap();
        m.putAll(map);
        String[] keys = (String[]) m.keySet().toArray(new String[0]);
//        String sKeys = "";
        for (String key : keys) {
            Object value;
            if (m.get(key).getClass().isArray()) {
                Object[] values = (Object[]) m.get(key);
                if (values.length > 1) {
                    value = values;
                } else {
                    value = values[0];
                }
            } else {
                value = m.get(key);
            }
            m.put(key, value);
        }
        return m;
    }
    private Map intermap = new HashMap();

    @Override
    public int size() {
        return intermap.size();
    }

    @Override
    public boolean isEmpty() {
        return intermap.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder postData = new StringBuilder();
        postData.append("Multipart:").append(isMultipart()).append(", Request Data:[");
        Set keys = this.keySet();
        for (Iterator<String> i = keys.iterator(); i.hasNext();) {
            String key = i.next();
            postData.append(key).append("=");
            if (this.get(key) instanceof File) {
                postData.append("[FILE]").append(this.get(key));
            } else {
                postData.append(this.get(key));
            }
            if (i.hasNext()) {
                postData.append("&");
            }
        }
        postData.append("]");
        return postData.toString();
    }

    @Override
    public boolean containsKey(Object key) {
        boolean result = false;
        try {
            Object tkey = (Object) key;
            result = intermap.containsKey(tkey);
        } catch (Exception e) {
        }
        return result;
    }

    @Override
    public boolean containsValue(Object value) {
        boolean result = false;
        try {
            Object tkey = (Object) value;
            result = intermap.containsValue(tkey);
            if (tkey.getClass().isArray() && !result) {
                intermap.containsKey(Arrays.asList(tkey));
            }
        } catch (Exception e) {
        }
        return result;
    }

    public String getParameter(String key) {
        return (String) get(key);
    }

    @Override
    public Object get(Object key) {
        Object result = null;
        try {
            result = intermap.get(key);
        } catch (Exception e) {
        }
        return result;

    }

    @Override
    public Object put(Object key, Object value) {
        return intermap.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        Object result = null;
        try {
            Object tkey = (Object) key;
            result = (Object) intermap.remove(tkey);
            if (result instanceof List) {
                result = ((List) result).toArray();
            }
        } catch (Exception e) {
        }
        return result;
    }

    @Override
    public void putAll(Map m) {
        intermap.putAll(m);
    }

    @Override
    public void clear() {
        intermap.clear();
    }

    @Override
    public Set<Object> keySet() {
        return intermap.keySet();
    }

    @Override
    public Collection<Object> values() {
        return intermap.values();
    }

    @Override
    public Set<Entry<Object, Object>> entrySet() {
        return intermap.entrySet();
    }

    private void putAsArray(Object key, Object value) {
        Object o = intermap.get(key);
        if (o != null) {
            if (o.getClass().isArray()) {
                try {
                    Object x = Array.newInstance(Array.get(o, 0).getClass(), Array.getLength(o) + 1);
                    for (int i = 0; i < Array.getLength(o); i++) {
                        Array.set(x, i, Array.get(o, i));
                    }
                    Array.set(x, Array.getLength(x) - 1, value);
                    put(key, x);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                return;
            } else {
                if (o.getClass() == value.getClass()) {
                    Object x = Array.newInstance(o.getClass(), 2);
                    Array.set(x, 0, o);
                    Array.set(x, 1, value);
                    put(key, x);
                    return;
                }
            }
        }
        put(key, value);
    }

    private int incFile(File file) {
        int result = 0;
        final File tf = file;
        File parent = file.getParentFile();
        if (parent.isDirectory()) {
            File[] files = parent.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    String start = tf.getName().substring(0, tf.getName().lastIndexOf("."));
                    String ext = tf.getAbsolutePath().substring(tf.getAbsolutePath().lastIndexOf("."));
                    return pathname.getName().startsWith(start) && pathname.getName().endsWith(ext);
                }
            });
            for (File f : files) {
                String tempFile = f.getName();
                String ext = f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("."));
                try {
                    Integer tmp = null;
                    try {
                        tmp = new Integer(tempFile.substring(0, tempFile.lastIndexOf(ext)));
                        if (tmp instanceof Integer) {
                            tmp = 0;
                        }
                    } catch (NumberFormatException e) {
                        String toParse = tempFile.substring(tempFile.lastIndexOf("_") + 1, tempFile.lastIndexOf(ext));
                        tmp = new Integer(toParse);
                    }

                    result = tmp > result ? tmp : result;
                } catch (NumberFormatException e) {
                }
            }
        }
        return result + 1;
    }

    private void processUploadedFile(FileItem item) throws Exception {
        String fieldName = item.getFieldName();
        String fileName = item.getName();
//        String contentType = item.getContentType();
//        boolean isInMemory = item.isInMemory();
//        long sizeInBytes = item.getSize();
        File uploadedFile = new File(Env.getConfig().getProperty(Env.FILE_UPLOAD_LOCATION, "") + "/" + fileName);
        try {
            if (uploadedFile.exists()) {
                String rename = uploadedFile.getAbsolutePath().substring(0, uploadedFile.getAbsolutePath().lastIndexOf(".")) + "_" + incFile(uploadedFile) + uploadedFile.getAbsolutePath().substring(uploadedFile.getAbsolutePath().lastIndexOf("."));
//                boolean renamesuccess = uploadedFile.renameTo(new File(rename));
                uploadedFile = new File(rename);
//                if (renamesuccess) {
//                    
//                }
            }
            item.write(uploadedFile);
        } catch (Exception e) {
            return;
        }
        putAsArray(fieldName, uploadedFile);
    }

    private void processFormField(FileItem item) {
        String name = item.getFieldName();
        String value = item.getString();
        putAsArray(name, value);
    }
}
