/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author lenovo
 */
public abstract class Application {

    public HttpServletRequest request;
    public HttpServletResponse response;

    public Application() {
        request = Env.getContext().getHttpServletRequest();
        response = Env.getContext().getHttpServletResponse();
    }

    public void flashData(String key, Object val) {
        Env.getContext().flashData(key, val);
    }

    public void addAttribute(String name, Object val) {
        request.setAttribute(name, val);
    }

    private static Logger logger = Logger.getLogger(Application.class);

    public abstract String index();
}
