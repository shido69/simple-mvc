/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx;

import com.shido.sfx.util.Env;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Shido69
 */
//@WebServlet(
//        name = "InitServlet",
//        urlPatterns = "/initConfig",
//        loadOnStartup = 2,
//        initParams = {
//            @WebInitParam(name = "configuration_file", value = "config.properties"),
//            @WebInitParam(name = "log_configuration_file", value = "config_log4j.properties")
//        }
//)
public class GenericServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(GenericServlet.class);
    private static final String LOG_CONFIG_INIT_PARAMETER = "log_configuration_file";
    private static final String CONFIG_INIT_PARAMETER = "configuration_file";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String logProperties = "/WEB-INF/" + getServletConfig().getInitParameter(GenericServlet.LOG_CONFIG_INIT_PARAMETER);
        String logConfigFile = getServletContext().getInitParameter("log-config-file");
        logConfigFile = logConfigFile == null ? "" : "/WEB-INF/" + logConfigFile;
        File fLogProperties = new File(logProperties = config.getServletContext().getRealPath(logConfigFile.isEmpty() ? logProperties : logConfigFile));
//            System.out.println(logProperties);
        if (fLogProperties.exists()) {
            PropertyConfigurator
                    .configure(logProperties);
        } else {
            System.out.println("Config file not found! Using console appender!");
            ConsoleAppender console = new ConsoleAppender();
            String PATTERN = "%d %p[%C{1} %L] %m%n";
            console.setLayout(new PatternLayout(PATTERN));
            console.setThreshold(Level.INFO);
            console.setName("console");
            console.activateOptions();
            Logger.getRootLogger().addAppender(console);
        }

        String configFile = getServletContext().getInitParameter("config-file");
        configFile = configFile == null ? "" : configFile;
        String configLocation = "/WEB-INF/" + (!configFile.isEmpty() ? configFile : getServletConfig().getInitParameter(GenericServlet.CONFIG_INIT_PARAMETER));
//        System.out.println(configLocation);
        logger.info("Loading config " + configLocation);
        try {
            loadMVCProperties(config.getServletContext().getResourceAsStream(configLocation));
        } catch (IOException | NullPointerException ex) {
            logger.debug(ex);
            logger.error("No Config File! Find config using Application interface...");
            try {
                String classpath = config.getServletContext().getRealPath("/WEB-INF/classes");
                Class[] apps = ClassScanner.getClasses(new File(classpath));
                for (Class app : apps) {
                    if (Configuration.class.isAssignableFrom(app)) {
                        System.out.println("config found! " + app.getName());
                        ((Configuration) app.newInstance()).init(config);
                    }
                }
            } catch (Exception e) {
                logger.error(e);
            }
            Scheduler scheduler = new Scheduler();
            Env.getCache().put(Env.SFX_SCHEDULER_OBJECT, scheduler);
        }
        logger.info("Config:\n" + Env.getConfig());
    }

    protected void loadMVCProperties(InputStream in) throws IOException {
        if (Env.getConfig() == null) {
            Env.setConfig(new Properties());
            Env.getConfig().load(in);
        }
        logger.info("Loaded Config:\n" + Env.getConfig());
    }

    public void destroy() {
        super.destroy();
    }
}
