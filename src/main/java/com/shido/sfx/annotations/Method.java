/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.annotations;

/**
 *
 * @author Shido69
 */
public enum Method {
    ALL,
    GET,
    POST,
    PUT,
    DELETE
}
