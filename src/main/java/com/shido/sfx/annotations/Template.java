/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Shido69
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Template {

    public String base() default "blank.jsp";

    public String title() default "";

    public String subContent() default "";

    public String sitePath() default "";

    public String contentPath() default "";

    public String additionalHeader() default "";

    public String additionalFooter() default "";
}
