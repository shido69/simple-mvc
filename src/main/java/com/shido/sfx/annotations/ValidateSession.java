/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shido.sfx.annotations;

/**
 *
 * @author lenovo
 */
public @interface ValidateSession {
    public String message();
    public String redirectUrl();
}
