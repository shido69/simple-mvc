# Welcome to SimpleMVC Framework. #

This Web/MVC Framework created by my own to simplify my work. Why reinvent the wheel?
I just want a simple MVC framework with more controll on what its process.

Don't get me wrong, Other web framework such as SpringMVC or any popular existing web framework are good.. but too many step to start a project and also too many class that I don't use.

### What is this repository for? ###
* MVC Framework (a simple one)
* ORM, mapping a database table to a class
* Scheduler, execute a class that implement Runnable with specifict time or schedule
* Many utility classes (commonly used by me)

### How do I get set up? ###
* compile and install the pom file to your local repo and add dependency to your maven web project.
```
<dependency>
	<groupId>com.shido</groupId>
	<artifactId>SimpleMVC</artifactId>
	<version>1.0-SNAPSHOT</version>
	<type>jar</type>
</dependency>
```

* create config.properties file on WEB-INF folder
* If you are using Servlet 3.0 you are already good, if not or you just want to override servlet mapping, put this Servlet Mapping on the web.xml

```
	<servlet>
        <servlet-name>dispatcher</servlet-name>
        <servlet-class>com.shido.sfx.MVCRequestHandler</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>dispatcher</servlet-name>
        <url-pattern>*.html</url-pattern>
    </servlet-mapping>
```

* add config entries below

** Mandatory **
```
	mvc.controller.package.location=com.shido.sfx.controller
	mvc.view.location=/WEB-INF/jsp
	mvc.file.upload.location=/tmp/sfx_temp

```
** For scheduler [Optional] **
```
	sfx.scheduler.name=Scheduler_Name
	#name of config for the scheduler
	sfx.scheduler.tasks=task1,task2,task3
	sfx.scheduler.interval=120000

	#daily task
	task1.type=daily
	#time to execute in 24 hour format separated using semicolon ";"
	task1.daily=03:00:00,10:00:00
	#class to execute (must implements Runnable interface)
	task1.daily.class=com.shido.empress.scheduler.PersonelTardiness

	#weekly task
	task2.type=weekly
	#time to execute separate using semicolon ";" entry format <day of week: 1 - 7>,<time in 24 hour>
	task2.weekly=2,05:00:00;4,05:00:00
	#class to execute (must implements Runnable interface)
	task2.weekly.class=com.shido.empress.scheduler.AttendancePin

	#monthly task
	task3.type=monthly
	#time to execute separate using semicolon ";" entry format <day of month>,<time in 24 hour>
	task3.monthly=20,05:00:00;10,05:00:00
	#class to execute (must implements Runnable interface)
	task3.monthly.class=
```

** For JDBC Connection [Optional] **
```
	jdbc.connections.settings=postgres,mysql

	jdbc.connection.postgres.user=db_user
	jdbc.connection.postgres.password=db_password
	jdbc.connection.postgres.driver=org.postgresql.Driver
	jdbc.connection.postgres.schema=public
	jdbc.connection.postgres.url=jdbc:postgresql://localhost:5432/DBName?currentSchema=public&autoReconnect=true&zeroDateTimeBehavior=convertToNull

	jdbc.connection.mysql.user=db_user
	jdbc.connection.mysql.password=db_password
	jdbc.connection.mysql.driver=com.mysql.jdbc.Driver
	jdbc.connection.mysql.schema=public
	jdbc.connection.mysql.url=jdbc:mysql://localhost:3306/DBName?autoReconnect=true&zeroDateTimeBehavior=convertToNull

```

* Just create a Class on the defined package location or use @Controller and @Route annotation 

```
package com.shido.sfx.controller

import com.shido.sfx.annotations.Controller;
import com.shido.sfx.annotations.Route;

@Controller
public class Application {
	@Route("/index")
	public String hello(){
		return "Hello World";
	}
}
```

### Annotations ###
These are some annotation being used by the framework
* MVC Related
```
@Controller, @Route, @Template, @ResponseBody, @URLParameter, @Request, @Response, @ServletConfig
```
* ORM Related
```
@Table, @Column, @PrimaryKey, @Join
```

### Misc ###
* Env class
Contains usefull framework functions such as Env.getBaseURI() function to get base context path.
* DBUtils class
Contains database related function to such as executing sql query or mapping it to an object or using collection base result;
* HttpUtils class
a Simple Http Client
* CommonUtils class
a set of common functions that commonly used by me

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

